package net.deludobellico.stfx.persistence.driver;

import net.deludobellico.stfx.model.gpx.Gpx;
import net.deludobellico.stfx.persistence.driver.DataDriver;
import net.deludobellico.stfx.persistence.driver.DataReader;
import net.deludobellico.stfx.persistence.driver.DataWriter;
import org.junit.Test;

import java.io.File;

/**
 * Created by mario on 19/05/2016.
 */
public class DataDriverTest {
    @Test
    public void testGpxDriver() throws Exception {
        DataDriver driver = DataDriver.getGpxDriver();
//        driver.loadDefaultDriverProperties();
        DataReader reader = driver.createReader();
        Gpx doc = reader.readGpxDocument(getClass().getClassLoader().getResourceAsStream("Oronet y La Frontera.gpx"));
        DataWriter writer = driver.createWriter();
//        File file = new File("DataDriverTest.gpx");
//        writer.testGpxDriver(doc, file.getPath());
//        Gpx doc2 = reader.readGpxDocument(file);
        File file = File.createTempFile("DataDriverTest", "gpx");
        writer.write(doc, file.getPath());
        Gpx doc2 = reader.readGpxDocument(file, true);
        file.delete();
    }

}