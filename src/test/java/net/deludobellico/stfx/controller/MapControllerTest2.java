package net.deludobellico.stfx.controller;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.deludobellico.stfx.analysis.TrackData;
import net.deludobellico.stfx.model.gpx.Gpx;
import net.deludobellico.stfx.persistence.driver.DataDriver;
import net.deludobellico.stfx.persistence.driver.DataReader;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import test.util.JFXTestRunner;
import test.util.JavaFXThreadingRule;

import static javafx.application.Platform.isFxApplicationThread;

/**
 * Created by mario on 23/06/2016.
 */
@RunWith(JFXTestRunner.class)
public class MapControllerTest2 {
    static MapController mapController;
//    @Rule
//    public JavaFXThreadingRule javafxRule = new JavaFXThreadingRule();

    public static class AsNonApp extends Application {
        @Override
        public void start(Stage stage) throws Exception {
            stage.initStyle(StageStyle.TRANSPARENT);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/MapView.fxml"));
            Parent root = fxmlLoader.load();
            mapController = fxmlLoader.getController();
            Scene scene = new Scene(root);
            scene.setFill(null);
            stage.setScene(scene);
            stage.show();
        }
    }

    @BeforeClass
    public static void initJFX() throws InterruptedException {
        Thread t = new Thread("JavaFX Init Thread") {
            public void run() {
                Application.launch(AsNonApp.class, new String[0]);
            }
        };
        t.setDaemon(true);
        t.start();
//        System.out.printf("FX App thread started\n");
//        Thread.sleep(10000);
    }


    @Test
    public void showTrackOnMap() throws Exception {
        assert isFxApplicationThread();
        DataDriver driver = DataDriver.getGpxDriver();
        DataReader reader = driver.createReader();
        Gpx doc = reader.readGpxDocument(getClass().getClassLoader().getResourceAsStream("Oronet y La Frontera.gpx"));
        TrackData trackData = new TrackData(doc.getTracks().get(0));
        mapController.showTrackOnMap(trackData);
    }
}