/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.deludobellico.stfx.util;

import net.deludobellico.stfx.analysis.TrackData;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Calendar;
import java.util.List;


/**
 *
 * @author Mario Gomez
 */
public class Worker {

    public static long generateDate(String FileName) {
        int Year = Integer.parseInt(FileName.substring(0, 4));
        int Month = Integer.parseInt(FileName.substring(4, 6));
        int Day = Integer.parseInt(FileName.substring(6, 8));
        int Hora = Integer.parseInt(FileName.substring(9, 11));
        int Minuto = Integer.parseInt(FileName.substring(11, 13));
        int Segundo = Integer.parseInt(FileName.substring(13, 15));

        Calendar cal = Calendar.getInstance();
        cal.set(Year, Month, Day, Hora, Minuto, Segundo);
        return cal.getTimeInMillis();
    }

    public static boolean compareDates(String FileName, int iMonth, int iYear) {
        int Year = Integer.parseInt(FileName.substring(0, 4));
        int Month = Integer.parseInt(FileName.substring(4, 6));

        return Year == iYear && Month == iMonth;
    }

    public static boolean compareDistances(String FileName, TrackData track, int minDistance, int maxDistance) {
        return (int) Worker.convertMetresToKM(track.getTotalDistance()) >= minDistance && (int) Worker.convertMetresToKM(track.getTotalDistance()) <= maxDistance;
    }

    public static boolean compareDurations(String FileName, TrackData track, int minDuration, int maxDuration) {
        return track.getTotalDuration().toMinutes() >= minDuration && track.getTotalDuration().toMinutes() <= maxDuration;
    }

    public static boolean compareDates(String FileName, int iYear) {
        int Year = Integer.parseInt(FileName.substring(0, 4));

        return Year == iYear;
    }

    public static String convertSegToDateHour(int num) {
        int hor, min, seg;
        hor = num / 3600;
        min = (num - (3600 * hor)) / 60;
        seg = num - ((hor * 3600) + (min * 60));

        return hor + ":" + min + ":" + seg;
    }

    public static double totalDistance(List<TrackData> tracks) {
        double aux = 0.0;
        for (TrackData t : tracks) {
            aux += t.getTotalDistance();
        }
        return aux;
    }

    public static int totalDuration(List<TrackData> tracks) {
        int aux = 0;
        for (TrackData t : tracks) {
            aux += t.getTotalDuration().toMinutes();
        }
        return aux;
    }

    public static double averageSpeed(List<TrackData> tracks) {
        double aux = 0.0;
        double aux2 = 0.0;
        for (TrackData t : tracks) {
            aux += t.getAverageSpeed();
            aux2++;
        }
        return aux / aux2;
    }

    public static String integerToMonth(int i) {
        switch (i + 1) {
            case 1:
                return "Enero";
            case 2:
                return "Febrero";
            case 3:
                return "Marzo";
            case 4:
                return "Abril";
            case 5:
                return "Mayo";
            case 6:
                return "Junio";
            case 7:
                return "Julio";
            case 8:
                return "Agosto";
            case 9:
                return "Septiembre";
            case 10:
                return "Octubre";
            case 11:
                return "Noviembre";
            case 12:
                return "Diciembre";
            default:
                return "";
        }
    }

    public static double convertMetresToKM(double m) {
        return m / 1000;
    }

    public static String generateTotalDuration(int m) {
        int hor = m / 3600;
        int min = (m - (3600 * hor)) / 60;
        int seg = m - ((hor * 3600) + (min * 60));
        return hor + ":" + min + ":" + seg;
    }

    public static double convertToKMH(double m) {
        return (m * 15) / 5;
    }

    public static void copyCustomTrack(File f) throws FileNotFoundException, IOException {
        File ff = new File("CustomTracks/" + f.getName());
        if(ff.exists())
            return;
        System.out.println(ff.getPath());
        Files.copy(f.toPath(), ff.toPath());
    }
}
