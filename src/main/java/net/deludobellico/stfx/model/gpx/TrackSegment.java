package net.deludobellico.stfx.model.gpx;

import java.util.ArrayList;
import java.util.List;

public class TrackSegment {
    private final List<Waypoint> waypoints = new ArrayList<>();

    public List<Waypoint> getWaypoints() {
        return this.waypoints;
    }
}
