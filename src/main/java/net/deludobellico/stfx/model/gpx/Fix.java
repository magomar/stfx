package net.deludobellico.stfx.model.gpx;

/**
 * Created by mario on 19/05/2016.
 */
public enum Fix {
    FIX_NONE("none"),
    FIX_2D("2d"),
    FIX_3D("3d"),
    FIX_DGPS("dgps"),
    FIX_PPS("pps");
    private final String name;

    Fix(String name) {
        this.name = name;
    }

    public static Fix fromString(String fix) {
        return Fix.valueOf("FIX_" + fix.toUpperCase());
    }


    @Override
    public String toString() {
        return name;
    }
}
