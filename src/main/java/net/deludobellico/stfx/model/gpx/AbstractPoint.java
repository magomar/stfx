package net.deludobellico.stfx.model.gpx;

import net.deludobellico.stfx.util.DateTimeUtils;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Calendar;

public abstract class AbstractPoint implements GeolocatedPoint {
    private double elevation;
    private double latitude;
    private double longitude;
    private LocalDateTime time;

    public AbstractPoint(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public AbstractPoint(double latitude, double longitude, double elevation) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.elevation = elevation;
    }

    public AbstractPoint(double latitude, double longitude, double elevation, LocalDateTime time) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.elevation = elevation;
        this.time = time;
    }

    public AbstractPoint(BigDecimal latitude, BigDecimal longitude, BigDecimal elevation, XMLGregorianCalendar time) {
        if (latitude == null || longitude == null)
            throw new IllegalArgumentException("Error creating AbstractPoint. Latitude, longitude and elevation must not be null");

        this.latitude = latitude.doubleValue();
        this.longitude = longitude.doubleValue();
        if (elevation != null) this.elevation = elevation.doubleValue();
        if (time != null) this.time = DateTimeUtils.toLocalDateTime(time);
    }

    @Override
    public double getElevation() {
        return elevation;
    }

    @Override
    public void setElevation(double elevation) {
        this.elevation = elevation;
    }

    @Override
    public double getLatitude() {
        return latitude;
    }

    @Override
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    @Override
    public double getLongitude() {
        return longitude;
    }

    @Override
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public LocalDateTime getTime() {
        return time;
    }

    @Override
    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    @Override
    public int compareTo(GeolocatedPoint o) {
        if (time == null) {
            if (o.getTime() == null)
                return 0;
            else
                return -1;
        } else {
            if (o.getTime() == null)
                return 1;
            else
                return this.getTime().compareTo(o.getTime());
        }
    }

    @Override
    public String toString() {
        return "lat: " + this.latitude + " lon: " + this.longitude + " elevation: " + this.elevation + " time: " + this.time;
    }
}
