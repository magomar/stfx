package net.deludobellico.stfx.model.gpx;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.EnumSet;
import java.util.List;

public class Waypoint extends AbstractPoint {

    private EnumSet<PointInfoType> infoTypes = EnumSet.of(PointInfoType.COORDINATES);

    private List<Link> links;

    private Degrees magvar;
    private double geoIdHeight;

    private PointDescription description;

    private PointAccuracy accuracy;

    private int cadence;

    private int heartRate;

    private int power;


    public Waypoint(BigDecimal latitude, BigDecimal longitude, BigDecimal elevation, XMLGregorianCalendar time) {
        super(latitude, longitude, elevation, time);
        if (elevation != null) infoTypes.add(PointInfoType.ELEVATION);
        if (time != null) infoTypes.add(PointInfoType.TIME);
    }

    public Waypoint(double latitude, double longitude) {
        super(latitude, longitude);
    }

    public Waypoint(double latitude, double longitude, double elevation) {
        super(latitude, longitude, elevation);
    }

    public Waypoint(double latitude, double longitude, double elevation, LocalDateTime time) {
        super(latitude, longitude, elevation, time);
    }

    public Degrees getMagvar() {
        return magvar;
    }

    public void setMagvar(Degrees magvar) {
        double degrees = magvar.getDegrees();
        if (degrees <-90 || degrees> 90)
            throw new IllegalArgumentException("Degrees must be greater than zero and less than 360");

        this.magvar = magvar;
        infoTypes.add(PointInfoType.MAGNETIC_VARIATION);
    }

    public double getGeoIdHeight() {
        return geoIdHeight;
    }

    public void setGeoIdHeight(double geoIdHeight) {
        this.geoIdHeight = geoIdHeight;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
        infoTypes.add(PointInfoType.LINKS);
    }

    public PointDescription getDescription() {
        return description;
    }

    public void setDescription(PointDescription description) {
        this.description = description;
        infoTypes.add(PointInfoType.DESCRIPTION);
    }

    public PointAccuracy getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(PointAccuracy accuracy) {
        this.accuracy = accuracy;
        infoTypes.add(PointInfoType.ACCURACY);
    }

    public int getCadence() {
        return cadence;
    }

    public void setCadence(int cadence) {
        this.cadence = cadence;
        infoTypes.add(PointInfoType.CADENCE);
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
        infoTypes.add(PointInfoType.HEART_RATE);
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
        infoTypes.add(PointInfoType.POWER);
    }

    public EnumSet<PointInfoType> getInfoTypes() {
        return infoTypes;
    }

    public boolean hasInfoType(PointInfoType infoType) {
        return infoTypes.contains(infoType);
    }
}