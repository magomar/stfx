package net.deludobellico.stfx.model.gpx;

import java.time.LocalDate;

public class Copyright {
    private LocalDate year;
    private String author;
    private String license;

    public Copyright(LocalDate year, String author, String license) {
        this.year = year;
        this.author = author;
        this.license = license;
    }

    public LocalDate getYear() {
        return year;
    }

    public String getAuthor() {
        return author;
    }

    public String getLicense() {
        return license;
    }

    @Override
    public String toString() {
        return this.author + " " + this.year + " " + this.license;
    }
}
