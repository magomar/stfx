package net.deludobellico.stfx.model.gpx;

import net.deludobellico.stfx.persistence.util.Constants;

import java.util.ArrayList;
import java.util.List;

public class Gpx {
    private final List<Track> tracks = new ArrayList<>();
    private final List<Waypoint> waypoints = new ArrayList<>();
    private final List<Route> routes = new ArrayList<>();
    private String version = Constants.APPLICATION_GPX_VERSION;
    private String creator = Constants.APPLICATION_NAME;
    private Metadata metadata;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public List<Track> getTracks() {
        return tracks;
    }

    public List<Waypoint> getWaypoints() {
        return waypoints;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }
}
