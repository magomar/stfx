package net.deludobellico.stfx.model.gpx;

import java.util.ArrayList;
import java.util.List;

public class PointsSequence {
    private final List<Point> points = new ArrayList<>();

    public List<Point> getPoints() {
        return points;
    }
}
