package net.deludobellico.stfx.model.gpx;

/**
 * Created by mario on 20/06/2016.
 */
public class PointDescription extends AbstractDescription {
    private String symbol;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
}
