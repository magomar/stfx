package net.deludobellico.stfx.model.gpx;

/**
 * Created by mario on 30/04/2016.
 */
public enum PointInfoType {
    COORDINATES,
    ELEVATION,
    TIME,
    MAGNETIC_VARIATION,
    GEOIDHEIGHT,
    HEART_RATE,
    CADENCE,
    POWER,
    DESCRIPTION,
    ACCURACY,
    DISTANCE,
    LINKS;
}
