package net.deludobellico.stfx.model.gpx;

/**
 * Created by mario on 21/06/2016.
 */
public class AbstractDescription {
    private String name;
    private String cmt;
    private String description;
    private String src;
    private String sym;
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCmt() {
        return cmt;
    }

    public void setCmt(String cmt) {
        this.cmt = cmt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getSymbol() {
        return sym;
    }

    public void setSymbol(String symbol) {
        this.sym = symbol;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
