package net.deludobellico.stfx.model.gpx;

import java.util.LinkedList;
import java.util.List;

public class Track {
    private final List<Link> links = new LinkedList<>();
    private final List<TrackSegment> trackSegments = new LinkedList<>();
    private int number;
    private TrackDescription description;
    private Waypoint start;
    private Waypoint end;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public TrackDescription getDescription() {
        return description;
    }

    public void setDescription(TrackDescription description) {
        this.description = description;
    }

    public List<Link> getLinks() {
        return links;
    }

    public List<TrackSegment> getTrackSegments() {
        return trackSegments;
    }

    public Waypoint getStart() {
        if (start == null) {
            start = trackSegments.get(0).getWaypoints().get(0);
        }
        return start;
    }

    public Waypoint getEnd() {
        if (end == null) {
            TrackSegment lastTrackSegment = trackSegments.get(trackSegments.size()- 1);
            List<Waypoint> waypoints = lastTrackSegment.getWaypoints();
            end = waypoints.get(waypoints.size() - 1);
        }
        return end;
    }
}
