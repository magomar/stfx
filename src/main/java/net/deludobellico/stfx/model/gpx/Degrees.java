package net.deludobellico.stfx.model.gpx;

public class Degrees {
    private double degrees;

    public Degrees(double degrees) throws IllegalArgumentException {
        if (degrees <0 || degrees> 360)
            throw new IllegalArgumentException("Degrees must be greater than zero and less than 360");

        this.degrees = degrees;
    }

    public double getDegrees() {
        return this.degrees;
    }
}
