package net.deludobellico.stfx.model.gpx;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Point extends AbstractPoint {

    public Point(BigDecimal latitude, BigDecimal longitude, BigDecimal elevation, XMLGregorianCalendar time) {
        super(latitude, longitude, elevation, time);
    }

    public Point(double latitude, double longitude) {
        super(latitude, longitude);
    }

    public Point(double latitude, double longitude, double elevation, LocalDateTime time) {
        super(latitude, longitude, elevation, time);
    }
}
