package net.deludobellico.stfx.model.gpx;

import java.time.LocalDateTime;

/**
 * Created by mario on 15/06/2016.
 */
public interface GeolocatedPoint extends Comparable<GeolocatedPoint> {
    double getElevation();

    void setElevation(double elevation);

    double getLatitude();

    void setLatitude(double latitude);

    double getLongitude();

    void setLongitude(double longitude);

    LocalDateTime getTime();

    void setTime(LocalDateTime time);
}
