package net.deludobellico.stfx.model.gpx;

public class DgpsStation {
    public static final int DGPS_STATION_MIN_VALUE = 0;
    public static final int DGPS_STATION_MAX_VALUE = 1023;

    private int dgpsStation;

    public DgpsStation(int dgpsStation) {
        if (dgpsStation < DGPS_STATION_MIN_VALUE || dgpsStation > DGPS_STATION_MAX_VALUE)
            throw new IllegalArgumentException("DgpsStation must be greater than " + DGPS_STATION_MIN_VALUE + " and less than " + DGPS_STATION_MAX_VALUE);

        this.dgpsStation = dgpsStation;
    }

    public DgpsStation(Integer dgpsStation){
        if(dgpsStation == null)
            throw new IllegalArgumentException("DgpsStation must be not null");
         else if (dgpsStation<DGPS_STATION_MIN_VALUE || dgpsStation>DGPS_STATION_MAX_VALUE)
            throw new IllegalArgumentException("DgpsStation must be greater than "+DGPS_STATION_MIN_VALUE+" and less than "+DGPS_STATION_MAX_VALUE);

        this.dgpsStation=dgpsStation;
    }

    public int getDgpsStation() {
        return dgpsStation;
    }

}
