package net.deludobellico.stfx.model.gpx;

import java.math.BigDecimal;

public class Bounds {
    private double minLatitude;
    private double minLongitude;
    private double maxLatitude;
    private double maxLongitude;

    public Bounds(double minLatitude, double minLongitude, double maxLatitude, double maxLongitude)  {
        this.minLatitude = minLatitude;
        this.minLongitude = minLongitude;
        this.maxLatitude = maxLatitude;
        this.maxLongitude = maxLongitude;
    }

    public Bounds(BigDecimal minLatitude, BigDecimal minLongitude, BigDecimal maxLatitude, BigDecimal maxLongitude) throws IllegalArgumentException {
        if (minLatitude == null || minLongitude == null || maxLatitude == null || maxLongitude == null)
            throw new IllegalArgumentException("Error creating bounds. Latitudes and longitudes must not be null");
        this.minLatitude = minLatitude.doubleValue();
        this.minLongitude = minLongitude.doubleValue();
        this.maxLatitude = maxLatitude.doubleValue();
        this.maxLongitude = maxLongitude.doubleValue();
    }

    public double getMinLatitude() {
        return minLatitude;
    }

    public double getMinLongitude() {
        return minLongitude;
    }

    public double getMaxLatitude() {
        return maxLatitude;
    }

    public double getMaxLongitude() {
        return maxLongitude;
    }

    @Override
    public String toString() {
        return "(" + minLatitude + ", " + minLongitude + ")-(" + maxLatitude + ", " + maxLongitude + ")";
    }
}
