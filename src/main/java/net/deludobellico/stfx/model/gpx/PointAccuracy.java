package net.deludobellico.stfx.model.gpx;

/**
 * Created by mario on 20/06/2016.
 */
public class PointAccuracy {
    private Fix fix;
    private int sat;
    private double hdop;
    private double vdop;
    private double pdop;
    private double ageOfDgpsData;
    private DgpsStation dGpsId;


    public Fix getFix() {
        return fix;
    }

    public void setFix(Fix fix) {
        this.fix = fix;
    }

    public int getSat() {
        return sat;
    }

    public void setSat(int sat) {
        this.sat = sat;
    }

    public double getHdop() {
        return hdop;
    }

    public void setHdop(double hdop) {
        this.hdop = hdop;
    }

    public double getVdop() {
        return vdop;
    }

    public void setVdop(double vdop) {
        this.vdop = vdop;
    }

    public double getPdop() {
        return pdop;
    }

    public void setPdop(double pdop) {
        this.pdop = pdop;
    }

    public double getAgeOfDgpsData() {
        return ageOfDgpsData;
    }

    public void setAgeOfDgpsData(double ageOfDgpsData) {
        this.ageOfDgpsData = ageOfDgpsData;
    }

    public DgpsStation getdGpsId() {
        return dGpsId;
    }

    public void setdGpsId(DgpsStation dGpsId) {
        this.dGpsId = dGpsId;
    }
}
