/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.deludobellico.stfx.model.manager;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import net.deludobellico.stfx.util.Worker;

/**
 *
 * @author Mario Gomez
 */
public class TrackView {

    private StringProperty name = new SimpleStringProperty();
    private StringProperty date = new SimpleStringProperty();
    private StringProperty distance = new SimpleStringProperty();
    private StringProperty duration = new SimpleStringProperty();
    private StringProperty speed = new SimpleStringProperty();

    public TrackView(String _date, Double _distance, Long _duration, Double _speed) {
        String DateStr = "Unknown";
        try {
            int Year = Integer.parseInt(_date.substring(0, 4));
            int Month = Integer.parseInt(_date.substring(4, 6));
            int Day = Integer.parseInt(_date.substring(6, 8));
            
            DateStr = Day + "." + Month + "." + Year;
        } catch (Exception e) { }

        this.name.setValue(_date);
        this.date.setValue(DateStr);
        this.distance.setValue(String.format("%.2f km", Worker.convertMetresToKM(_distance)));
        this.duration.setValue(_duration + " min");
        this.speed.setValue(String.format("%.2f km/h", _speed));
    }

    public String getName() {
        return name.get();
    }

    public void setName(String fName) {
        name.set(fName);
    }
    
    public String getDate() {
        return date.get();
    }

    public void setDate(String fName) {
        date.set(fName);
    }

    public String getDistance() {
        return distance.get();
    }

    public void setDistance(String fName) {
        distance.set(fName);
    }

    public String getDuration() {
        return duration.get();
    }

    public void setDuration(String fName) {
        duration.set(fName);
    }

    public String getSpeed() {
        return speed.get();
    }

    public void setSpeed(String fName) {
        speed.set(fName);
    }
}
