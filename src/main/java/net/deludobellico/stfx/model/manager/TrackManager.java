package net.deludobellico.stfx.model.manager;

import net.deludobellico.stfx.analysis.TrackData;
import net.deludobellico.stfx.exception.*;
import net.deludobellico.stfx.model.gpx.Gpx;
import net.deludobellico.stfx.persistence.driver.DataAdapter;
import net.deludobellico.stfx.persistence.driver.DataDriver;
import net.deludobellico.stfx.persistence.driver.DataReader;
import net.deludobellico.stfx.persistence.driver.gpx.GpxDataAdapter;
import net.deludobellico.stfx.util.Worker;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Mario Gomez
 */
public final class TrackManager {

    private Map<String, TrackData> tracks = new HashMap<>();
    private DataReader reader;
    private DataAdapter adapter;

    public TrackManager() throws JAXBException, DriverIOException, DriverFileNotFoundException, DriverReaderException, DriverPropertiesException, DriverValidationException {
        DataDriver driver = DataDriver.getGpxDriver();
        reader = driver.createReader();
        adapter = new GpxDataAdapter();
        this.loadTracks(reader, adapter);
    }

    public List<TrackView> getTrackViewFromYear(int Year) {
        List<TrackView> tracks = new ArrayList();
        for (Map.Entry<String, TrackData> entry : this.tracks.entrySet()) {
            if (Worker.compareDates(entry.getKey(), Year)) {
                TrackData t = entry.getValue();
                TrackView tv = new TrackView(entry.getKey(), t.getTotalDistance(), t.getTotalDuration().toMinutes(), t.getAverageSpeed());
                tracks.add(tv);
            }
        }
        return tracks;
    }

    public List<TrackView> getTrackViewFromMonth(int Month, int Year) {
        List<TrackView> tracks = new ArrayList();
        for (Map.Entry<String, TrackData> entry : this.tracks.entrySet()) {
            if (Worker.compareDates(entry.getKey(), Month, Year)) {
                TrackData t = entry.getValue();
                TrackView tv = new TrackView(entry.getKey(), t.getTotalDistance(), t.getTotalDuration().toMinutes(), t.getAverageSpeed());
                tracks.add(tv);
            }
        }
        return tracks;
    }

    public List<TrackView> getTrackViewFromDistance(int minDistance, int maxDistance) {
        List<TrackView> tracks = new ArrayList();
        for (Map.Entry<String, TrackData> entry : this.tracks.entrySet()) {
            if (Worker.compareDistances(entry.getKey(), entry.getValue(), minDistance, maxDistance)) {
                TrackData t = entry.getValue();
                TrackView tv = new TrackView(entry.getKey(), t.getTotalDistance(), t.getTotalDuration().toMinutes(), t.getAverageSpeed());
                tracks.add(tv);
            }
        }
        return tracks;
    }

    public List<TrackView> getTrackViewFromDuration(int minDuration, int maxDuration) {
        List<TrackView> tracks = new ArrayList();
        for (Map.Entry<String, TrackData> entry : this.tracks.entrySet()) {
            if (Worker.compareDurations(entry.getKey(), entry.getValue(), minDuration, maxDuration)) {
                TrackData t = entry.getValue();
                TrackView tv = new TrackView(entry.getKey(), t.getTotalDistance(), t.getTotalDuration().toMinutes(), t.getAverageSpeed());
                tracks.add(tv);
            }
        }
        return tracks;
    }

    public void loadTracks(DataReader reader, DataAdapter dataAdapter) throws JAXBException, DriverIOException, DriverValidationException, DriverPropertiesException, DriverReaderException, DriverFileNotFoundException {

//        File f = new File("Tracks/");
        File folder = new File("D:/Documents/Netbeans/Tracks");
        if (folder.exists()) {
            File[] ficheros = folder.listFiles();
            for (File file : ficheros) {
                Gpx gpx = reader.readGpxDocument(file);
                TrackData trackData = new TrackData(gpx.getTracks().get(0));
//                TrackData trackData = loadTrackDataFromFile(ff.getName());
                this.tracks.put(file.getName(), trackData);
            }
        } else {
            System.out.println("Error: " + folder.getPath() + " does not exist");
        }
    }

//    public TrackData loadTrackDataFromFile(String FileName) throws JAXBException {
//        File f = new File("Tracks/" + FileName);
//        JAXBContext jaxbContext = JAXBContext.newInstance(GpxType.class, TrackPointExtensionT.class);
//        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
//        JAXBElement<Object> root = (JAXBElement<Object>) unmarshaller.unmarshal(f);
//        GpxType gpx = (GpxType) root.getValue();
//
//        if (gpx != null) {
//            return new TrackData(new Track(gpx.getTrk().get(0)));
//        } else {
//            return null;
//        }
//    }
//
//    public TrackData loadTrackDataFromPathFile(String FileName) throws JAXBException {
//        File f = new File(FileName);
//        JAXBContext jaxbContext = JAXBContext.newInstance(GpxType.class, TrackPointExtensionT.class);
//        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
//        JAXBElement<Object> root = (JAXBElement<Object>) unmarshaller.unmarshal(f);
//        GpxType gpx = (GpxType) root.getValue();
//
//        if (gpx != null) {
//            return new TrackData(new Track(gpx.getTrk().get(0)));
//        } else {
//            return null;
//        }
//    }

    public TrackData loadTrackDataFromCache(String FileName) {
        return this.tracks.get(FileName);
    }

    public TrackData getLastTrack() throws JAXBException {
        String lastActivityName = "";
        long maximumMillisecs = 0, aux = 0;
        for (String date : this.tracks.keySet()) {
            aux = Worker.generateDate(date);
            if (maximumMillisecs < aux) {
                maximumMillisecs = aux;
                lastActivityName = date;
            }
        }
        return loadTrackDataFromCache(lastActivityName);
    }

    public List<TrackData> getMonthlyActivities(int Month, int Year) throws JAXBException {
        List<TrackData> tracks = new ArrayList();
        for (String date : this.tracks.keySet()) {
            if (Worker.compareDates(date, Month, Year)) {
                TrackData t = loadTrackDataFromCache(date);
                tracks.add(t);
            }
        }
        return tracks;
    }

    public int getTracksCountByMonth(int Month, int Year) throws JAXBException {
        int Count = 0;
        for (String date : this.tracks.keySet()) {
            if (Worker.compareDates(date, Month, Year)) {
                Count++;
            }
        }
        return Count;
    }

    public int getTracksCountByDistance(int minDistance, int maxDistance) throws JAXBException {
        int Count = 0;
        for (Map.Entry<String, TrackData> entry : this.tracks.entrySet()) {
            if (Worker.compareDistances(entry.getKey(), entry.getValue(), minDistance, maxDistance)) {
                Count++;
            }
        }
        return Count;
    }

    public int getTracksCountByDuration(int minDuration, int maxDuration) throws JAXBException {
        int Count = 0;
        for (Map.Entry<String, TrackData> entry : this.tracks.entrySet()) {
            if (Worker.compareDurations(entry.getKey(), entry.getValue(), minDuration, maxDuration)) {
                Count++;
            }
        }
        return Count;
    }

    public List<TrackData> getTracksByYear(int Year) throws JAXBException {
        List<TrackData> tracks = new ArrayList();
        for (String date : this.tracks.keySet()) {
            if (Worker.compareDates(date, Year)) {
                TrackData t = loadTrackDataFromCache(date);
                tracks.add(t);
            }
        }
        return tracks;
    }

    public double getTracksDistanceByMonth(int Month, int Year) throws JAXBException {
        double Distance = 0;
        for (String date : this.tracks.keySet()) {
            if (Worker.compareDates(date, Month, Year)) {
                Distance += tracks.get(date).getTotalDistance();
            }
        }
        return Distance;
    }

    public int getTracksDurationByMonth(int Month, int Year) throws JAXBException {
        int Duration = 0;
        for (String date : this.tracks.keySet()) {
            if (Worker.compareDates(date, Month, Year)) {
                Duration += tracks.get(date).getTotalDuration().toHours();
            }
        }
        return Duration;
    }

    public int getTracksVMByMonth(int Month, int Year) throws JAXBException {
        int VM = 0;
        int Count = 0;
        for (String date : this.tracks.keySet()) {
            if (Worker.compareDates(date, Month, Year)) {
                VM += tracks.get(date).getAverageSpeed();
                Count++;
            }
        }
        if (Count == 0) {
            return 0;
        }
        return VM / Count;
    }

    public int getTracksCountByYear(int Year) throws JAXBException {
        int Count = 0;
        for (String date : this.tracks.keySet()) {
            if (Worker.compareDates(date, Year)) {
                Count++;
            }
        }
        return Count;
    }

    public double getTotalDistanceByYear(int Year) throws JAXBException {
        double Distance = 0;
        for (String date : this.tracks.keySet()) {
            if (Worker.compareDates(date, Year)) {
                Distance += tracks.get(date).getTotalDistance();
            }
        }
        return Distance;
    }

    public int getTotalDurationByYear(int Year) throws JAXBException {
        int Duration = 0;
        for (String date : this.tracks.keySet()) {
            if (Worker.compareDates(date, Year)) {
                Duration += tracks.get(date).getTotalDuration().toMinutes();
            }
        }
        return Duration;
    }

    public double getTotalVMByYear(int Year) throws JAXBException {
        double averageSpeed = 0;
        for (String date : this.tracks.keySet()) {
            if (Worker.compareDates(date, Year)) {
                averageSpeed += tracks.get(date).getAverageSpeed();
            }
        }
        return averageSpeed;
    }

    public Map<String, TrackData> getTracks() {
        return tracks;
    }

    public DataReader getReader() {
        return reader;
    }

    public DataAdapter getAdapter() {
        return adapter;
    }
}
