package net.deludobellico.stfx.model.manager;

import java.util.List;

/**
 * Created by mario on 26/06/2016.
 */
public class UserProfile {
    private int localId;
    /**
     * Athlete's first name
     */
    private String firstname;
    /**
     * Athlete's last name
     */
    private String lastname;

    private String nickname;

    private String pwd;

    private List<Activity> activities;
}
