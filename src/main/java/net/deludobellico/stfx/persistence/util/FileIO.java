package net.deludobellico.stfx.persistence.util;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class controls all access to disk and stores resources paths.
 */
public class FileIO {

    private static final String SETTINGS_XML_FILE = "settings.xml";
    private static final Logger LOG = Logger.getLogger(FileIO.class.getName());
    private static final boolean IS_DEPLOYED;

    static {
        Path userPath = FileSystems.getDefault().getPath(System.getProperty("user.dir"));
        IS_DEPLOYED = userPath.endsWith("bin");
        LOG.log(Level.INFO, "User dir = " + userPath.toString() + " -- Is deployed? " + IS_DEPLOYED);
    }

//    /**
//     * JAXB paths
//     */
//    private static final String JAXB_CONTEXT_PATH = "net.deludobellico.stfx.persistence.gpx";
//    private static JAXBContext JAXB_CONTEXT;
//    private static Marshaller MARSHALLER;
//    private static Unmarshaller UNMARSHALLER;
//
//    static {
//        try {
//            JAXB_CONTEXT = JAXBContext.newInstance(JAXB_CONTEXT_PATH);
//            UNMARSHALLER = JAXB_CONTEXT.createUnmarshaller();
//            MARSHALLER = JAXB_CONTEXT.createMarshaller();
//            MARSHALLER.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//        } catch (JAXBException e) {
//            e.printStackTrace();
//        }
//    }

    /**
     * Protect the class from being instantiated
     */
    private FileIO() {
    }

    public static boolean isDeployed() {
        return IS_DEPLOYED;
    }

    /**
     * Gets the list of files in a given directory, using certain filter, and with the option to search the
     * directory recursively.
     */

    public static List<File> listFiles(File directory, FilenameFilter filter, boolean recursively) {
        List<File> files = new ArrayList<>();
        if (directory.isDirectory()) {
            File[] entries = directory.listFiles();
            for (File entry : entries) {
                if (filter == null || filter.accept(directory, entry.getName())) {
                    files.add(entry);
                }
                if (recursively && entry.isDirectory()) {
                    files.addAll(listFiles(entry, filter, recursively));
                }
            }
        } else {
            LOG.severe(String.format("Error listing files. %s is not a directory", directory));
        }
        return files;
    }

    /**
     * Gets a file given a string description of a relative path, or various relative paths to be appended in order
     *
     * @param relativePath
     * @return
     */
    public static File getFile(String... relativePath) {
        Path path = getPath(relativePath);
        return getFileOrCreateNew(path.toString());
    }

    /**
     * Gets a file given an absolute path, or creates a new file if no file exists in that path
     *
     * @param path
     * @return
     */
    private static File getFileOrCreateNew(String path) {
        File f = null;
        try {
            f = new File(path);
            boolean exists = f.exists();
            if (!exists) {
                boolean createNew = f.createNewFile();
                if (!createNew) {
                    throw new IOException();
                }
            }
        } catch (IOException e) {
            LOG.log(Level.WARNING, "Error creating: " + path, new IOException());
        }
        return f;
    }

    /****
     * Gets an absolute path given a string description of a relative path, or various relative paths to be appended in order
     *
     * @param relativePath
     * @return
     */
    public static Path getPath(String... relativePath) {
        String[] paths;
        if (IS_DEPLOYED) {
            int numPaths = relativePath.length + 1;
            paths = new String[numPaths];
            paths[0] = "..";
            for (int i = 1; i < paths.length; i++) {
                paths[i] = relativePath[i - 1];
            }
        } else {
            paths = relativePath;
        }
        return FileSystems.getDefault().getPath(System.getProperty("user.dir"), paths);
    }

//    public static Path getResourcesPath(String module, String... relativePath) {
//        String[] paths = new String[relativePath.length + 2];
//        paths[0] = module;
//        paths[1] = FileIO.RESOURCES_FOLDER;
//        for (int i = 2; i < paths.length; i++) {
//            paths[i] = relativePath[i - 2];
//        }
//        return getPath(paths);
//    }

    /**
     * Copy one file to another
     *
     * @param sourceFile
     * @param targetFile
     */
    public static void copy(File sourceFile, File targetFile) {
        try {
            Files.copy(sourceFile.toPath(), targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Removes file extension from a string description of a path
     *
     * @param filePath
     * @return
     */
    public static String removeExtention(String filePath) {
        File f = new File(filePath);
        // if it's a directory there is no extension to remove
        if (f.isDirectory()) return filePath;
        String name = f.getName();
        final int lastPeriodPos = name.lastIndexOf('.');
        if (lastPeriodPos <= 0) {
            // No period after first character - return name as it was passed in
            return filePath;
        } else {
            // Remove the last period and everything after it
            File renamed = new File(f.getParent(), name.substring(0, lastPeriodPos));
            return renamed.getPath();
        }
    }

    /**
     * Gets the file extension of a given file
     *
     * @param file
     * @return
     */
    public static String getExtension(File file) {
        if (file.isDirectory()) return "";
        String name = file.getName();
        final int lastPeriodPos = name.lastIndexOf('.');
        if (lastPeriodPos <= 0) {
            // No period after first character - return name as it was passed in
            return "";
        } else {
            return name.substring(lastPeriodPos + 1);
        }
    }


    /******* MARSHALLING / UNMARSHALLING  *******/

//    public static <T> T unmarshallFile(File file, Class<T> jaxbClass) {
//        T object = null;
//        try {
//            Source source = new StreamSource(file);
//            JAXBElement<T> root = UNMARSHALLER.unmarshal(source, jaxbClass);
//            object = root.getValue();
//        } catch (JAXBException ex) {
//            LOG.log(Level.SEVERE, "Exception unmarshalling XML", ex);
//        } finally {
//            return object;
//        }
//    }
//
//    public static File marshallXML(Object object, File file) {
//        try {
//            try (FileOutputStream fos = new FileOutputStream(file)) {
//                MARSHALLER.marshal(object, fos);
//                return file;
//            } catch (IOException ex) {
//                LOG.log(Level.SEVERE, "Exception marshalling XML", ex);
//            }
//        } catch (JAXBException ex) {
//            LOG.log(Level.SEVERE, null, ex);
//        }
//        return null;
//    }
//
//    public static StringWriter marshallXML(Object object) {
//        try (StringWriter sw = new StringWriter()) {
//            MARSHALLER.marshal(object, sw);
//            return sw;
//        } catch (IOException | JAXBException ex) {
//            LOG.log(Level.SEVERE, "Exception marshalling XML", ex);
//        }
//        return null;
//    }
//
//    public static <T> T unmarshallZip(File file, Class<T> jaxbClass) {
//        T object = null;
//        try (ZipFile zipFile = new ZipFile(file.getPath())) {
//            ZipEntry zipEntry = zipFile.entries().nextElement();
//            InputStream inputStream = zipFile.getInputStream(zipEntry);
//            JAXBElement<T> root = UNMARSHALLER.unmarshal(new StreamSource(inputStream), jaxbClass);
//            object = root.getValue();
//        } catch (JAXBException ex) {
//            LOG.log(Level.SEVERE, "Exception unmarshalling XML", ex);
//        } finally {
//            return object;
//        }
//    }
//
//    public static File marshallZipped(Object object, File file) {
//        File zipFile = new File(file.getAbsolutePath() + ".zip");
//        try {
//            try {
//                FileOutputStream fos = new FileOutputStream(zipFile);
//                try (ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(fos))) {
//                    ZipEntry ze = new ZipEntry(file.getName());
//                    zos.putNextEntry(ze);
//                    MARSHALLER.marshal(object, zos);
//                    return zipFile;
//                } catch (IOException ex) {
//                    LOG.log(Level.SEVERE, "Exception marshalling XML", ex);
//                }
//            } catch (FileNotFoundException ex) {
//                LOG.log(Level.SEVERE, "Exception marshalling XML", ex);
//            }
//
//        } catch (JAXBException ex) {
//            LOG.log(Level.SEVERE, "Exception marshalling XML", ex);
//        }
//        return null;
//    }
//
//    public static File marshallGzipped(Object object, File file) {
//        File gzFile = new File(file.getAbsolutePath() + ".gz");
//        try {
//            try {
//                FileOutputStream fos = new FileOutputStream(gzFile);
//                try {
//                    GZIPOutputStream gz = new GZIPOutputStream(fos);
//                    MARSHALLER.marshal(object, gz);
//                    return gzFile;
//                } catch (IOException ex) {
//                    LOG.log(Level.SEVERE, "Exception marshalling XML", ex);
//                }
//            } catch (FileNotFoundException ex) {
//                LOG.log(Level.SEVERE, "Exception marshalling XML", ex);
//            }
//
//        } catch (JAXBException ex) {
//            LOG.log(Level.SEVERE, "Exception marshalling XML", ex);
//        }
//        return null;
//    }
//
//    /**
//     * Unmarshalls Json element of type {@code c} from the {@code file}.
//     *
//     * @param c    the class of object to be unmarshalled
//     * @param file the Json file containing the marshalled object
//     * @return the object of type {@code T} from the {@code file}
//     */
//    public static <T> T unmarshallJson(File file, Class<T> c) {
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
//        T object = null;
//        try {
//            object = mapper.readValue(file, c);
//        } catch (IOException ex) {
//            LOG.log(Level.SEVERE, "Exception unmarshalling Json", ex);
//        }
//        return object;
//    }
//
//    /**
//     * Marshalls Java object into a Json file
//     *
//     * @param object object to be marshalled
//     * @param file   file to save the marshalled object
//     * @return
//     */
//    public static File marshallJson(Object object, File file) {
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
//        ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
//        try {
//            writer.writeValue(file, object);
//            return file;
//        } catch (IOException ex) {
//            LOG.log(Level.SEVERE, "Exception marshalling Json", ex);
//        }
//        return null;
//    }

}