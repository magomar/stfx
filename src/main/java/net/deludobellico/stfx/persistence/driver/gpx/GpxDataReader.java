package net.deludobellico.stfx.persistence.driver.gpx;

import net.deludobellico.stfx.exception.*;
import net.deludobellico.stfx.model.gpx.Gpx;
import net.deludobellico.stfx.persistence.driver.DataAdapter;
import net.deludobellico.stfx.persistence.driver.DataDriver;
import net.deludobellico.stfx.persistence.driver.DataReader;
import net.deludobellico.stfx.persistence.jaxb.gpx.GpxType;
import net.deludobellico.stfx.persistence.jaxb.gpx.TrackPointExtensionT;
import net.deludobellico.stfx.persistence.util.Constants;
import net.deludobellico.stfx.persistence.util.CopyInputStream;
import net.deludobellico.stfx.persistence.util.JAXBFactory;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

public class GpxDataReader implements DataReader {

    private final DataAdapter adapter;
    private final Properties driverProp;
    private final Validator validator;
    private JAXBFactory jaxbFactory;

    public GpxDataReader()  {
        DataDriver driver = DataDriver.getGpxDriver();
        this.driverProp = driver.getProperties();
        this.adapter = new GpxDataAdapter();
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = null;
        try {
            schema = schemaFactory.newSchema(GpxDataReader.class.getClassLoader().getResource(Constants.APPLICATION_DEFAULT_GPX_DTD_1_1_FILENAME));
        } catch (SAXException e) {
            URL url = null;
            try {
                url = new URL(this.driverProp.getProperty(Constants.DRIVER_XSD_URL_GPX_1_1));
                schema = schemaFactory.newSchema(new StreamSource(url.openStream()));
            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            } catch (SAXException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }
        validator = schema.newValidator();
        try {
            jaxbFactory = new JAXBFactory(GpxType.class, TrackPointExtensionT.class);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }


    @Override
    public Gpx readGpxDocument(String filepath, boolean validateDocument) throws DriverFileNotFoundException, DriverIOException, DriverReaderException, DriverValidationException {
        return this.readGpxDocument(new File(filepath), validateDocument);
    }

    @Override
    public Gpx readGpxDocument(String filepath) throws DriverFileNotFoundException, DriverIOException, DriverReaderException, DriverPropertiesException, DriverValidationException {
        return this.readGpxDocument(new File(filepath));
    }

    @Override
    public Gpx readGpxDocument(File input, boolean validateDocument) throws DriverFileNotFoundException, DriverIOException, DriverReaderException, DriverValidationException {
        try {
            return this.readGpxDocument(new FileInputStream(input), validateDocument);
        } catch (FileNotFoundException e) {
            throw new DriverFileNotFoundException(e.getMessage());
        }
    }

    @Override
    public Gpx readGpxDocument(File input) throws DriverFileNotFoundException, DriverIOException, DriverReaderException, DriverPropertiesException, DriverValidationException {
        Properties properties = DataDriver.getGpxDriver().getProperties();
        if (properties == null)
            throw new DriverPropertiesException("Driver properties not loaded. Please load properties from driver");

        return this.readGpxDocument(input, Boolean.valueOf(properties.getProperty(Constants.DRIVER_VALIDATE_GPX_FILE, "false")));
    }


    @Override
    public Gpx readGpxDocument(InputStream input) throws DriverIOException, DriverReaderException, DriverPropertiesException, DriverValidationException {
        if (driverProp == null)
            throw new DriverPropertiesException("Driver properties not loaded. Please load properties from driver");
        return this.readGpxDocument(input, Boolean.valueOf(driverProp.getProperty(Constants.DRIVER_VALIDATE_GPX_FILE, "false")));
    }

    @Override
    public Gpx readGpxDocument(InputStream input, boolean validateDocument) throws DriverIOException, DriverReaderException, DriverValidationException {
        CopyInputStream cis = new CopyInputStream(input);
        InputStream input1 = cis.getCopy();
        InputStream input2;
        Gpx gpx;
        try {
            JAXBContext jc = JAXBContext.newInstance(GpxType.class.getPackage().getName());
            Unmarshaller u = jc.createUnmarshaller();
            if (validateDocument) {
                input2 = cis.getCopy(); // We need two copies to validate,  because the validation process
                // will close the input stream, so it cannot be used again to read the file
                validator.validate(new StreamSource(input2));
            }
            GpxType gpxDoc = (GpxType) jaxbFactory.unmarshallStream(input1);
            gpx = adapter.toGpxDocument(gpxDoc);
            return gpx;
        } catch (JAXBException e) {
            throw new DriverReaderException(e);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
