package net.deludobellico.stfx.persistence.driver.gpx;

import net.deludobellico.stfx.model.gpx.*;
import net.deludobellico.stfx.persistence.driver.DataAdapter;
import net.deludobellico.stfx.persistence.jaxb.gpx.*;
import net.deludobellico.stfx.util.DateTimeUtils;
import sun.awt.image.ImageWatched;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class GpxDataAdapter implements DataAdapter {

    @Override
    public Bounds toBounds(Object bounds) {
        if (bounds == null)
            return null;
        BoundsType bType = (BoundsType) bounds;
        Bounds b = new Bounds(bType.getMinlat(), bType.getMinlon(),
                bType.getMaxlat(), bType.getMaxlon());
        return b;
    }

    @Override
    public Object fromBounds(Bounds bounds) {
        if (bounds == null)
            return null;
        BoundsType bType = new BoundsType();
        bType.setMinlat(BigDecimal.valueOf(bounds.getMinLatitude()));
        bType.setMinlon(BigDecimal.valueOf(bounds.getMinLongitude()));
        bType.setMaxlat(BigDecimal.valueOf(bounds.getMaxLatitude()));
        bType.setMaxlon(BigDecimal.valueOf(bounds.getMaxLongitude()));
        return bType;
    }

    @Override
    public Copyright toCopyright(Object copyright) {
        if (copyright == null)
            return null;
        CopyrightType cType = (CopyrightType) copyright;
        Copyright c = new Copyright((cType.getYear() == null) ? null : DateTimeUtils.toLocalDate(cType.getYear()), cType.getAuthor(), cType.getLicense());
        return c;
    }

    @Override
    public Object fromCopyright(Copyright copyright) {
        if (copyright == null)
            return null;
        CopyrightType cType = new CopyrightType();
        cType.setAuthor(copyright.getAuthor());
        cType.setLicense(copyright.getLicense());
        cType.setYear((copyright.getYear() == null) ? null : DateTimeUtils.toXMLGregorianCalendar(copyright.getYear()));
        return cType;
    }

    @Override
    public Degrees toDegrees(Object degrees) {
        if (degrees == null)
            return null;
        return new Degrees(((BigDecimal) degrees).doubleValue());
    }

    @Override
    public Object fromDegrees(Degrees degrees) {
        if (degrees == null)
            return null;
        return degrees.getDegrees();
    }

    @Override
    public DgpsStation toDgpsStation(Object dgpsStation) {
        if (dgpsStation == null)
            return null;
        return new DgpsStation((Integer) dgpsStation);
    }

    @Override
    public Object fromDgpsStation(DgpsStation dgpsStation) {
        if (dgpsStation == null)
            return null;
        return dgpsStation.getDgpsStation();
    }

    @Override
    public Email toEmail(Object email) {
        if (email == null)
            return null;
        EmailType eType = (EmailType) email;
        Email e = new Email(eType.getId(), eType.getDomain());
        return e;
    }

    @Override
    public Object fromEmail(Email email) {
        if (email == null)
            return null;
        EmailType eType = new EmailType();
        eType.setId(email.getUser());
        eType.setDomain(email.getDomain());
        return eType;
    }

    @Override
    public Fix toFix(Object fix) {
        if (fix == null)
            return null;

        return Fix.fromString((String) fix);
    }

    @Override
    public Object fromFix(Fix fix) {
        if (fix == null)
            return null;
        return fix.toString();
    }

    @Override
    public Gpx toGpxDocument(Object gpxDocument) {
        if (gpxDocument == null)
            return null;
        GpxType gpxType = (GpxType) gpxDocument;
        Gpx gpx = new Gpx();
        gpx.setVersion(gpxType.getVersion());
        gpx.setCreator(gpxType.getCreator());
        gpx.setMetadata(this.toMetadata(gpxType.getMetadata()));
        for (WptType waypoint : gpxType.getWpt()) {
            gpx.getWaypoints().add(toWaypoint(waypoint));
        }
        for (RteType route : gpxType.getRte()) {
            gpx.getRoutes().add(toRoute(route));
        }
        for (TrkType track : gpxType.getTrk()) {
            gpx.getTracks().add(toTrack(track));
        }
        // TODO extensions
        return gpx;
    }

    @Override
    public Object fromGpxDocument(Gpx gpx) {
        if (gpx == null)
            return null;
        GpxType gpxType = new GpxType();
        gpxType.setVersion(gpx.getVersion());
        gpxType.setCreator(gpx.getCreator());
        gpxType.setMetadata((MetadataType) this.fromMetadata(gpx.getMetadata()));
        for (Waypoint waypoint : gpx.getWaypoints()) {
            gpxType.getWpt().add((WptType) fromWaypoint(waypoint));
        }
        for (Route route : gpx.getRoutes()) {
            gpxType.getRte().add((RteType) fromRoute(route));
        }
        for (Track track : gpx.getTracks()) {
            gpxType.getTrk().add((TrkType) fromTrack(track));
        }
        // TODO extensions
        return gpxType;
    }

    @Override
    public Link toLink(Object link) {
        if (link == null)
            return null;
        LinkType linkType = (LinkType) link;
        Link l = new Link();
        try {
            l.setHref(new URL(linkType.getHref()));
            l.setText(linkType.getText());
            l.setType(linkType.getType());
            return l;
        } catch (MalformedURLException e) {
            return null;
        }
    }

    @Override
    public Object fromLink(Link link) {
        if (link == null)
            return null;

        LinkType linkType = new LinkType();
        linkType.setHref((link.getHref() == null) ? null : link.getHref().toString());
        linkType.setText(link.getText());
        linkType.setType(link.getType());

        return linkType;
    }

    @Override
    public Metadata toMetadata(Object metadata) {
        if (metadata == null)
            return null;
        MetadataType metadataType = (MetadataType) metadata;
        Metadata m = new Metadata();
        m.setName(metadataType.getName());
        m.setDesc(metadataType.getDesc());
        m.setAuthor(this.toPerson(metadataType.getAuthor()));
        m.setCopyright(this.toCopyright(metadataType.getCopyright()));
        m.setTime((metadataType.getTime() == null) ? null : DateTimeUtils.toLocalDateTime(metadataType.getTime()));
        m.setKeywords(metadataType.getKeywords());
        m.setBounds(this.toBounds(metadataType.getBounds()));
        for (LinkType link : metadataType.getLink()) {
            m.getLinks().add(toLink(link));
        }
        return m;
    }

    @Override
    public Object fromMetadata(Metadata metadata) {
        if (metadata == null)
            return null;
        MetadataType mType = new MetadataType();
        mType.setAuthor((PersonType) this.fromPerson(metadata.getAuthor()));
        mType.setBounds((BoundsType) this.fromBounds(metadata.getBounds()));
        mType.setCopyright((CopyrightType) this.fromCopyright(metadata.getCopyright()));
        mType.setDesc(metadata.getDesc());
        mType.setKeywords(metadata.getKeywords());
        mType.setName(metadata.getName());
        mType.setTime((metadata.getTime() == null) ? null : DateTimeUtils.toXMLGregorianCalendar(metadata.getTime()));
        for (Link link : metadata.getLinks()) {
            mType.getLink().add((LinkType) fromLink(link));
        }
        // TODO extensions
        return mType;
    }

    @Override
    public Person toPerson(Object person) {
        if (person == null)
            return null;
        PersonType personType = (PersonType) person;
        Person p = new Person();
        p.setName(personType.getName());
        p.setLink(this.toLink(personType.getLink()));
        p.setEmail(this.toEmail(personType.getEmail()));

        return p;
    }

    @Override
    public Object fromPerson(Person person) {
        if (person == null)
            return null;
        PersonType personType = new PersonType();
        personType.setName(person.getName());
        personType.setEmail((EmailType) this.fromEmail(person.getEmail()));
        personType.setLink((LinkType) this.fromLink(person.getLink()));
        return personType;
    }

    @Override
    public Point toPoint(Object point) {
        if (point == null)
            return null;
        PtType pointType = (PtType) point;

        Point p = new Point(pointType.getLat().doubleValue(), pointType.getLon().doubleValue(),
                pointType.getEle().doubleValue(),
                (pointType.getTime() == null ? null : DateTimeUtils.toLocalDateTime(pointType.getTime())));
        return p;
    }

    @Override
    public Object fromPoint(Point point) {
        if (point == null)
            return null;
        PtType pType = new PtType();
        pType.setLat(BigDecimal.valueOf(point.getLatitude()));
        pType.setLon(BigDecimal.valueOf(point.getLongitude()));
        pType.setEle(BigDecimal.valueOf(point.getElevation()));
        pType.setTime((point.getTime() == null) ? null : DateTimeUtils.toXMLGregorianCalendar(point.getTime()));
        return pType;
    }

    @Override
    public PointsSequence toPointSequence(Object pointSegment) {
        if (pointSegment == null)
            return null;
        PtsegType pointSegmentType = (PtsegType) pointSegment;
        PointsSequence ps = new PointsSequence();
        for (PtType point : pointSegmentType.getPt()) {
            ps.getPoints().add(toPoint(point));
        }
        return ps;
    }

    @Override
    public Object fromPointSequence(PointsSequence pointSegment) {
        if (pointSegment == null)
            return null;
        PtsegType pointSegmentType = new PtsegType();
        for (Point point : pointSegment.getPoints()) {
            pointSegmentType.getPt().add((PtType) fromPoint(point));
        }
        return pointSegmentType;
    }

    @Override
    public Route toRoute(Object route) {
        if (route == null)
            return null;

        RteType routeType = (RteType) route;
        Route r = new Route();
        r.setName(routeType.getName());
        r.setCmt(routeType.getCmt());
        r.setDesc(routeType.getDesc());
        r.setSrc(routeType.getSrc());
        r.setNumber(routeType.getNumber().intValue());
        r.setType(routeType.getType());
        for (WptType waypoint : routeType.getRtept()) {
            r.getWaypoints().add(toWaypoint(waypoint));
        }
        for (LinkType linkType : routeType.getLink()) {
            r.getLinks().add(toLink(linkType));
        }
        // TODO extensions
        return r;
    }

    @Override
    public Object fromRoute(Route route) {
        if (route == null)
            return null;
        RteType routeType = new RteType();
        routeType.setName(route.getName());
        routeType.setCmt(route.getCmt());
        routeType.setDesc(route.getDesc());
        routeType.setSrc(route.getSrc());
        routeType.setNumber(BigInteger.valueOf(route.getNumber()));
        routeType.setType(route.getType());
        for (Waypoint waypoint : route.getWaypoints()) {
            routeType.getRtept().add((WptType) fromWaypoint(waypoint));
        }
        for (Link link : route.getLinks()) {
            routeType.getLink().add((LinkType) fromLink(link));
        }
        // TODO extensions
        return routeType;
    }

    @Override
    public Track toTrack(Object track) {
        if (track == null)
            return null;
        TrkType trackType = (TrkType) track;
        Track t = new Track();
        TrackDescription description = new TrackDescription();
        description.setName(trackType.getName());
        description.setCmt(trackType.getCmt());
        description.setDescription(trackType.getDesc());
        description.setSrc(trackType.getSrc());
        description.setType(trackType.getType());
        t.setDescription(description);
        if (trackType.getNumber() != null)         t.setNumber(trackType.getNumber().intValue());

        for (TrksegType trackSegmentType : trackType.getTrkseg()) {
            t.getTrackSegments().add(toTrackSegment(trackSegmentType));
        }
        for (LinkType linkType : trackType.getLink()) {
            t.getLinks().add(toLink(linkType));
        }
        // TODO extensions
        return t;
    }

    @Override
    public Object fromTrack(Track track) {
        if (track == null)
            return null;
        TrkType trackType = new TrkType();
        TrackDescription description = track.getDescription();
        trackType.setName(description.getName());
        trackType.setCmt(description.getCmt());
        trackType.setDesc(description.getDescription());
        trackType.setSrc(description.getSrc());
        trackType.setType(description.getType());
        trackType.setNumber(BigInteger.valueOf(track.getNumber()));
        for (TrackSegment trackSegment : track.getTrackSegments()) {
            trackType.getTrkseg().add((TrksegType) fromTrackSegment(trackSegment));
        }
        for (Link link : track.getLinks()) {
            trackType.getLink().add((LinkType) fromLink(link));
        }
        // TODO extensions
        return trackType;
    }

    @Override
    public TrackSegment toTrackSegment(Object trackSegment) {
        if (trackSegment == null)
            return null;
        TrksegType trackSegmentType = (TrksegType) trackSegment;
        TrackSegment t = new TrackSegment();
        for (WptType waypoint : trackSegmentType.getTrkpt()) {
            t.getWaypoints().add(toWaypoint(waypoint));
        }
        // TODO extensions
        return t;
    }

    @Override
    public Object fromTrackSegment(TrackSegment trackSegment) {
        if (trackSegment == null)
            return null;
        TrksegType trackSegmentType = new TrksegType();
        for (Waypoint waypoint : trackSegment.getWaypoints()) {
            trackSegmentType.getTrkpt().add((WptType) fromWaypoint(waypoint));
        }
        // TODO extensions
        return trackSegmentType;
    }

    @Override
    public Waypoint toWaypoint(Object waypoint) {
        if (waypoint == null)
            return null;
        WptType waypointType = (WptType) waypoint;
        Waypoint w = new Waypoint(waypointType.getLat(), waypointType.getLon(), waypointType.getEle(),waypointType.getTime());
        // TODO finish toWaypoint
//        w.setMagvar(this.toDegrees(waypointType.getMagvar()));
//        w.setGeoIdHeight(waypointType.getGeoidheight().doubleValue());
//
//        PointDescription description = new PointDescription();
//        description.setName(waypointType.getName());
//        description.setCmt(waypointType.getCmt());
//        description.setDescription(waypointType.getDesc());
//        description.setSrc(waypointType.getSrc());
//        description.setSymbol(waypointType.getSymbol());
//        description.setType(waypointType.getType());
//        w.setDescription(description);
//
//        PointAccuracy accuracy = new PointAccuracy();
//        accuracy.setFix(this.toFix(waypointType.getFix()));
//        accuracy.setSat(waypointType.getSat().intValue());
//        accuracy.setHdop(waypointType.getHdop().doubleValue());
//        accuracy.setVdop(waypointType.getVdop().doubleValue());
//        accuracy.setPdop(waypointType.getPdop().doubleValue());
//        accuracy.setAgeOfDgpsData(waypointType.getAgeofdgpsdata().doubleValue());
//        accuracy.setdGpsId(this.toDgpsStation(waypointType.getDgpsid()));
//        w.setAccuracy(accuracy);
//
//        w.setLinks(waypointType.getLink().stream().map(this::toLink).collect(Collectors.toCollection(LinkedList::new)));
        // TODO extensions
        return w;
    }

    @Override
    public Object fromWaypoint(Waypoint waypoint) {
        if (waypoint == null)
            return null;
        WptType waypointType = new WptType();
        waypointType.setLat(BigDecimal.valueOf(waypoint.getLatitude()));
        waypointType.setLon(BigDecimal.valueOf(waypoint.getLongitude()));
        waypointType.setEle(BigDecimal.valueOf(waypoint.getElevation()));
        waypointType.setTime((waypoint.getTime() == null) ? null : DateTimeUtils.toXMLGregorianCalendar(waypoint.getTime()));
        //TODO finish fromWaypoint
//        waypointType.setMagvar((BigDecimal) this.fromDegrees(waypoint.getMagvar()));
//        waypointType.setGeoidheight(BigDecimal.valueOf(waypoint.getGeoIdHeight()));
//
//        PointDescription description = waypoint.getDescription();
//        waypointType.setName(description.getName());
//        waypointType.setCmt(description.getCmt());
//        waypointType.setDesc(description.getDescription());
//        waypointType.setSrc(description.getSrc());
//        waypointType.setSymbol(description.getSymbol());
//        waypointType.setType(description.getType());
//
//        PointAccuracy accuracy = waypoint.getAccuracy();
//        waypointType.setFix((String) this.fromFix(accuracy.getFix()));
//        waypointType.setSat(BigInteger.valueOf(accuracy.getSat()));
//        waypointType.setHdop(BigDecimal.valueOf(accuracy.getHdop()));
//        waypointType.setVdop(BigDecimal.valueOf(accuracy.getVdop()));
//        waypointType.setPdop(BigDecimal.valueOf(accuracy.getPdop()));
//        waypointType.setAgeofdgpsdata(BigDecimal.valueOf(accuracy.getAgeOfDgpsData()));
//        waypointType.setDgpsid((Integer) this.fromDgpsStation(accuracy.getdGpsId()));
//
//        for (Link link : waypoint.getLinks()) {
//            waypointType.getLink().add((LinkType) fromLink(link));
//        }
        // TODO extensions
        return waypointType;
    }

}