package net.deludobellico.stfx.persistence.driver;


import net.deludobellico.stfx.exception.*;
import net.deludobellico.stfx.model.gpx.Gpx;

import java.io.File;
import java.io.InputStream;


public interface DataReader {
    Gpx readGpxDocument(String filepath) throws DriverValidationException, DriverFileNotFoundException, DriverIOException, DriverReaderException, DriverPropertiesException;

    Gpx readGpxDocument(String filepath, boolean validateDocument) throws DriverValidationException, DriverFileNotFoundException, DriverIOException, DriverReaderException;

    Gpx readGpxDocument(File input, boolean validateDocument) throws DriverValidationException, DriverFileNotFoundException, DriverIOException, DriverReaderException;

    Gpx readGpxDocument(File input) throws DriverValidationException, DriverFileNotFoundException, DriverIOException, DriverReaderException, DriverPropertiesException;

    Gpx readGpxDocument(InputStream input, boolean validateDocument) throws DriverValidationException, DriverIOException, DriverReaderException;

    Gpx readGpxDocument(InputStream input) throws DriverValidationException, DriverIOException, DriverReaderException, DriverPropertiesException;
}
