package net.deludobellico.stfx.persistence.driver;

import net.deludobellico.stfx.exception.DriverFileNotFoundException;
import net.deludobellico.stfx.exception.DriverIOException;
import net.deludobellico.stfx.exception.DriverPropertiesException;
import net.deludobellico.stfx.exception.DriverWriterException;
import net.deludobellico.stfx.model.gpx.Gpx;

import java.io.OutputStream;

public interface DataWriter {
    void write(Gpx doc, String filePath) throws DriverIOException, DriverPropertiesException, DriverFileNotFoundException, DriverWriterException;

    void write(Gpx doc, OutputStream output) throws DriverPropertiesException, DriverIOException, DriverWriterException;

    String writeToString(Gpx doc) throws DriverPropertiesException, DriverWriterException;
}
