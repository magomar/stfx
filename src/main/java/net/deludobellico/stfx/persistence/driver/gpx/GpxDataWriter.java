package net.deludobellico.stfx.persistence.driver.gpx;

import net.deludobellico.stfx.exception.DriverFileNotFoundException;
import net.deludobellico.stfx.exception.DriverIOException;
import net.deludobellico.stfx.exception.DriverPropertiesException;
import net.deludobellico.stfx.exception.DriverWriterException;
import net.deludobellico.stfx.model.gpx.Gpx;
import net.deludobellico.stfx.persistence.driver.DataDriver;
import net.deludobellico.stfx.persistence.driver.DataWriter;
import net.deludobellico.stfx.persistence.jaxb.gpx.GpxType;
import net.deludobellico.stfx.persistence.util.Constants;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import java.io.*;
import java.util.Properties;

public class GpxDataWriter implements DataWriter {
    
    private final DataDriver driver;
    private final Properties properties;

    public GpxDataWriter() throws DriverFileNotFoundException, DriverPropertiesException {
        this.driver = DataDriver.getGpxDriver();
        this.properties = this.driver.getProperties();
    }

    @Override
    public void write(Gpx doc, String filePath) throws DriverIOException, DriverPropertiesException, DriverFileNotFoundException, DriverWriterException {
        try {
            this.write(doc, new FileOutputStream(filePath));
        } catch (FileNotFoundException e) {
            throw new DriverFileNotFoundException(e.getMessage());
        }
    }

    @Override
    public void write(Gpx doc, OutputStream output) throws DriverPropertiesException, DriverIOException, DriverWriterException {
        String formatted = writeToString(doc);
        OutputStreamWriter out = null;
        try {
            out = new OutputStreamWriter(output, "UTF-8");
            out.write(formatted);
        } catch (UnsupportedEncodingException e1) {
            throw new DriverWriterException(e1);
        } catch (IOException e) {
            throw new DriverIOException(e);
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                throw new DriverIOException(e);
            }
        }
    }

    @Override
    public String writeToString(Gpx doc) throws DriverPropertiesException, DriverWriterException {
        if (this.properties == null)
            throw new DriverPropertiesException("Driver properties not loaded. Please load properties from driver");

        try {
            JAXBContext jc = JAXBContext.newInstance(GpxType.class.getPackage().getName());
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.valueOf(this.properties.getProperty(Constants.DRIVER_WRITER_NEW_LINE, "true")));
            marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, this.properties.getProperty(Constants.DRIVER_XSD_URL_GPX_1_1));
            marshaller.setProperty(Marshaller.JAXB_ENCODING, this.properties.getProperty(Constants.DRIVER_WRITER_ENCODING, "UTF-8"));
//            marshaller.setProperty("com.sun.xml.bind.indentString", this.appProperties.getProperty(Constants.DRIVER_WRITER_INDENTATION_TEXT, "\t"));
//            marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new JaxbNamespacePrefixMapper());

            StringWriter sw = new StringWriter();
            JAXBElement<GpxType> element = new JAXBElement<>(new QName("http://www.topografix.com/GPX/1/1", "gpx"), GpxType.class, (GpxType) new GpxDataAdapter().fromGpxDocument(doc));
            marshaller.marshal(element, sw);

            return sw.toString();
        } catch (JAXBException e) {
            throw new DriverWriterException(e);
        }
    }

}
