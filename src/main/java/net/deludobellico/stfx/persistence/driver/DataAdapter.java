package net.deludobellico.stfx.persistence.driver;

import net.deludobellico.stfx.model.gpx.*;

public interface DataAdapter {
    Bounds toBounds(Object bounds);

    Object fromBounds(Bounds bounds);

    Copyright toCopyright(Object copyright);

    Object fromCopyright(Copyright copyright);

    Degrees toDegrees(Object degrees);

    Object fromDegrees(Degrees degrees);

    DgpsStation toDgpsStation(Object dgpsStation);

    Object fromDgpsStation(DgpsStation dgpsStation);

    Email toEmail(Object email);

    Object fromEmail(Email email);

    Fix toFix(Object fix);

    Object fromFix(Fix fix);

    Gpx toGpxDocument(Object gpxDocument);

    Object fromGpxDocument(Gpx document);

    Link toLink(Object link);

    Object fromLink(Link link);

    Metadata toMetadata(Object metadata);

    Object fromMetadata(Metadata metadata);

    Person toPerson(Object person);

    Object fromPerson(Person person);

    Point toPoint(Object point);

    Object fromPoint(Point point);

    PointsSequence toPointSequence(Object pointSequence);

    Object fromPointSequence(PointsSequence pointsSequence);

    Route toRoute(Object route);

    Object fromRoute(Route route);

    Track toTrack(Object track);

    Object fromTrack(Track track);

    TrackSegment toTrackSegment(Object trackSegment);

    Object fromTrackSegment(TrackSegment trackSegment);

    Waypoint toWaypoint(Object waypoint);

    Object fromWaypoint(Waypoint waypoint);
}
