package net.deludobellico.stfx.persistence.driver;

import net.deludobellico.stfx.exception.*;
import net.deludobellico.stfx.persistence.util.Constants;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DataDriver {
    private static DataDriver driver = null;
    private DataReader reader;
    private DataWriter writer;
    private Properties properties;

    public static DataDriver getGpxDriver() {
        if (driver == null)
            driver = new DataDriver();
        return driver;
    }


    public void loadDriverProperties(String filePath) throws DriverIOException {
        try {
            this.loadDriverProperties(new FileInputStream(filePath));
        } catch (IOException e) {
            throw new DriverIOException(e);
        }
    }

    public void loadDriverProperties(InputStream input) throws DriverIOException {
        properties = new Properties();
        try {
            properties.load(input);
        } catch (IOException e) {
            throw new DriverIOException(e);
        }
    }

    public void loadDriverProperties(Properties properties) {
        if (properties == null)
            throw new IllegalArgumentException("Error loading driver properties. Properties must not be null");
        this.properties = properties;
    }

    public void loadDefaultDriverProperties() throws DriverFileNotFoundException, DriverIOException {
        try {
            loadDriverProperties(DataDriver.class.getClassLoader().getResourceAsStream(Constants.APPLICATION_DEFAULT_DRIVER_PROPERTIES_FILENAME));
        } catch (IOException e) {
            throw new DriverIOException("Error loading default driver properties.", e);
        }
    }
    public Properties getProperties() {
        return properties;
    }

    public DataReader getReader() throws DriverReaderException, DriverPropertiesException {
        if (reader == null) {
            String className = getProperties().getProperty(Constants.DRIVER_READER_CLASS_NAME);
            if (className == null)
                throw new DriverReaderException("Property " + Constants.DRIVER_READER_CLASS_NAME + " not found in properties file");
            try {
                this.reader = (DataReader) Class.forName(className).newInstance();
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                throw new DriverReaderException(e);
            }
        }
        return this.reader;
    }

    public DataReader createReader() throws DriverReaderException, DriverPropertiesException, DriverIOException, DriverFileNotFoundException {
        if (this.properties == null)
            loadDefaultDriverProperties();
        String className = this.properties.getProperty(Constants.DRIVER_READER_CLASS_NAME);
        if (className == null)
            throw new DriverReaderException("Property " + Constants.DRIVER_READER_CLASS_NAME + " not found in properties file");
        return this.createReader(className);
    }

    public DataReader createReader(String classname) throws DriverReaderException, IllegalArgumentException, ClassCastException {
        if (classname == null)
            throw new IllegalArgumentException("Error creating reader from a classname. Classname must not be null");
        try {
            reader = (DataReader) Class.forName(classname).newInstance();
            return reader;
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            throw new DriverReaderException(e);
        }
    }

    public DataWriter getWriter() throws DriverWriterException, DriverPropertiesException {
        if (this.writer == null) {
            if (this.properties == null)
                throw new DriverPropertiesException("Driver properties not loaded. Please load properties before use the writer");
            String className = this.properties.getProperty(Constants.DRIVER_WRITER_CLASS_NAME);
            if (className == null)
                throw new DriverWriterException("Property " + Constants.DRIVER_WRITER_CLASS_NAME + " not found in properties file");
            try {
                this.writer = (DataWriter) Class.forName(className).newInstance();
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                throw new DriverWriterException(e);
            }
        }
        return this.writer;
    }

    public DataWriter createWriter() throws DriverWriterException, DriverPropertiesException {
        String className = this.getProperties().getProperty(Constants.DRIVER_WRITER_CLASS_NAME);
        if (className == null)
            throw new DriverWriterException("Property " + Constants.DRIVER_WRITER_CLASS_NAME + " not found in properties file");

        return this.createWriter(className);
    }

    public DataWriter createWriter(String classname) throws DriverWriterException {
        if (classname == null)
            throw new IllegalArgumentException("Error creating writer from a classname. Classname must not be null");
        try {
            writer = (DataWriter) Class.forName(classname).newInstance();
            return writer;
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            throw new DriverWriterException(e);
        }
    }
}
