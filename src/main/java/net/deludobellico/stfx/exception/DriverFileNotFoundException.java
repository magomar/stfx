package net.deludobellico.stfx.exception;

import java.io.FileNotFoundException;

public class DriverFileNotFoundException extends FileNotFoundException {

    /**
     *
     */
    private static final long serialVersionUID = 6585173525939830804L;

    public DriverFileNotFoundException(String message) {
        super(message);
    }

    public DriverFileNotFoundException() {
        super();
    }
}
