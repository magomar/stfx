package net.deludobellico.stfx.exception;

public class DriverWriterException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 5205797111016461086L;

    public DriverWriterException() {
        super();
    }

    public DriverWriterException(String message) {
        super(message);
    }

    public DriverWriterException(String message, Throwable t) {
        super(message, t);
    }

    public DriverWriterException(Throwable t) {
        super(t);
    }
}
