package net.deludobellico.stfx.exception;

public class DriverPropertiesException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 5387343352310214627L;

    public DriverPropertiesException() {
        super();
    }

    public DriverPropertiesException(String message) {
        super(message);
    }

    public DriverPropertiesException(Throwable t) {
        super(t);
    }

    public DriverPropertiesException(String message, Throwable t) {
        super(message, t);
    }
}
