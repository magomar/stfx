package net.deludobellico.stfx.exception;

public class DriverReaderException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 3137364129788613594L;

    public DriverReaderException() {
        super();
    }

    public DriverReaderException(String message) {
        super(message);
    }

    public DriverReaderException(String message, Throwable t) {
        super(message, t);
    }

    public DriverReaderException(Throwable clazz) {
        super(clazz);
    }
}
