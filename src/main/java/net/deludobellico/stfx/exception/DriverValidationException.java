package net.deludobellico.stfx.exception;

public class DriverValidationException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -6461604431011677165L;

    public DriverValidationException() {
        super();
    }

    public DriverValidationException(String message) {
        super(message);
    }

    public DriverValidationException(String message, Throwable t) {
        super(message, t);
    }

    public DriverValidationException(Throwable t) {
        super(t);
    }
}
