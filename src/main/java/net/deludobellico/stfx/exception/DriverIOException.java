package net.deludobellico.stfx.exception;

import java.io.IOException;

public class DriverIOException extends IOException {

    /**
     *
     */
    private static final long serialVersionUID = -4386355490863575429L;

    public DriverIOException() {
        super();
    }

    public DriverIOException(String message) {
        super(message);
    }

    public DriverIOException(String message, Throwable t) {
        super(message, t);
    }

    public DriverIOException(Throwable t) {
        super(t);
    }
}
