/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.deludobellico.stfx.controller;

import net.deludobellico.stfx.STFXApp;
import net.deludobellico.stfx.model.manager.TrackManager;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 *
 * @author Mario Gomez
 */
public class LoadingController implements Initializable {

    @FXML
    private Label lbl;


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.LoadingBucle();
    }

    private void LoadingBucle() {
        Thread th = new Thread(task);
        th.setDaemon(true);
        th.start();
        
        task.setOnSucceeded(e->{
            showActivityMethod();
        });
    }

    Task<Void> task = new Task<Void>() {
        @Override
        protected Void call() throws Exception {
            STFXApp.trackManager = new TrackManager();
            return null;
        }
    };

    private void showActivityMethod() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/SummaryView.fxml"));
            Parent root = loader.load();
            Scene scene = new Scene(root);
            Stage estageActual = new Stage();
            estageActual.setScene(scene);
            estageActual.show();
            
            Stage stage = (Stage) lbl.getScene().getWindow();
            stage.close();
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }
}
