/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.deludobellico.stfx.controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.*;
import javafx.scene.control.*;
import net.deludobellico.stfx.util.Worker;
import net.deludobellico.stfx.analysis.TrackData;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author Mario Gomez
 */
public class GraphicsDocumentController implements Initializable {

    private TrackData trackdata;
    private Boolean isDistance;
    private int chartSplit;
    private XYChart.Series velocidadSerie;
    private XYChart.Series frecuenciaSerie;
    private XYChart.Series cadenciaSerie;

    @FXML
    private AreaChart areachart;

    @FXML
    private NumberAxis xAxis;

    @FXML
    private NumberAxis yAxis;

    @FXML
    private LineChart linechart;

    @FXML
    private NumberAxis xAxis2;

    @FXML
    private NumberAxis yAxis2;

    @FXML
    private ToggleGroup group;

    @FXML
    private RadioButton radDist;

    @FXML
    private RadioButton radTime;

    @FXML
    private ComboBox combobox;

    @FXML
    private PieChart piechart;

    @FXML
    private CheckBox checkV;

    @FXML
    private CheckBox checkF;

    @FXML
    private CheckBox checkC;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

        group.selectedToggleProperty().addListener(
                (ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
                    if (group.getSelectedToggle() != null) {
                        isDistance = radDist.isSelected();
                        setComboBox();
                    }
                }
        );

        checkV.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (checkV.isSelected()) {
                    linechart.getData().add(velocidadSerie);
                } else {
                    linechart.getData().remove(velocidadSerie);
                }
            }
        });

        checkF.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (checkF.isSelected()) {
                    linechart.getData().add(frecuenciaSerie);
                } else {
                    linechart.getData().remove(frecuenciaSerie);
                }
            }
        });

        checkC.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (checkC.isSelected()) {
                    linechart.getData().add(cadenciaSerie);
                } else {
                    linechart.getData().remove(cadenciaSerie);
                }
            }
        });

        ObservableList<String> options
                = FXCollections.observableArrayList(
                "Altura x Distancia",
                "Velocidad x Distancia",
                "FC x Distancia",
                "Cadencia x Distancia",
                "Esfuerzo cardíaco",
                "Combinación"
        );

        combobox.setItems(options);
        combobox.setValue("Altura x Distancia");
    }

    public void loadInformation(TrackData data) {
        this.trackdata = data;
        this.isDistance = true;
        this.chartSplit = this.trackdata.getChunks().size() / 200;

        this.setAreaChart();
    }

    private void setAreaChart() {
        xAxis.autosize();
        yAxis.autosize();
        areachart.getData().clear();
        areachart.setLegendVisible(false);
        XYChart.Series statsProfile = new XYChart.Series();

        if (isDistance) {
            for (int i = 0; i < this.trackdata.getNumPoints(); i = i + this.chartSplit) {
                double distPoint = Worker.convertMetresToKM(i * (this.trackdata.getTotalDistance() / this.trackdata.getNumPoints()));
                statsProfile.getData().add(new XYChart.Data(distPoint, trackdata.getChunks().get(i).getAvgHeight()));
            }
            xAxis.setLabel("Distancia (km)");
            areachart.getData().add(statsProfile);
        } else {
            for (int i = 0; i < this.trackdata.getNumPoints(); i = i + this.chartSplit) {
                double timePoint = i * (this.trackdata.getTotalDuration().toMinutes() / (double) this.trackdata.getNumPoints());
                statsProfile.getData().add(new XYChart.Data(timePoint, this.trackdata.getChunks().get(i).getAvgHeight()));
            }
            xAxis.setLabel("Tiempo (min)");
            areachart.getData().add(statsProfile);
        }
        yAxis.setLabel("Altura");
    }

    private void setLineChart(int lineChartValue) {
        yAxis2.autosize();
        xAxis2.autosize();
        XYChart.Series statsProfile = new XYChart.Series();
        linechart.getData().clear();
        linechart.setLegendVisible(false);
        if (isDistance) {
            for (int i = 0; i < this.trackdata.getNumPoints(); i = i + this.chartSplit) {
                double distancePoint = Worker.convertMetresToKM(i * (this.trackdata.getTotalDistance() / this.trackdata.getNumPoints()));
                if (lineChartValue == 1) {
                    statsProfile.getData().add(new XYChart.Data(distancePoint, Worker.convertToKMH(this.trackdata.getChunks().get(i).getSpeed())));
                } else if (lineChartValue == 2) {
                    statsProfile.getData().add(new XYChart.Data(distancePoint, this.trackdata.getChunks().get(i).getAvgHeartRate()));
                } else if (lineChartValue == 3) {
                    statsProfile.getData().add(new XYChart.Data(distancePoint, this.trackdata.getChunks().get(i).getAvgCadence()));
                }
            }
            xAxis2.setLabel("Distancia (km)");
        } else {
            for (int i = 0; i < this.trackdata.getNumPoints(); i = i + this.chartSplit) {
                double timePoint = i * (this.trackdata.getTotalDuration().toMinutes() / (double) this.trackdata.getNumPoints());
                if (lineChartValue == 1) {
                    statsProfile.getData().add(new XYChart.Data(timePoint, Worker.convertToKMH(this.trackdata.getChunks().get(i).getSpeed())));
                } else if (lineChartValue == 2) {
                    statsProfile.getData().add(new XYChart.Data(timePoint, this.trackdata.getChunks().get(i).getAvgHeartRate()));
                } else if (lineChartValue == 3) {
                    statsProfile.getData().add(new XYChart.Data(timePoint, this.trackdata.getChunks().get(i).getAvgCadence()));
                }
            }
            xAxis2.setLabel("Tiempo (min)");
        }
        linechart.getData().add(statsProfile);
    }

    private void setPieChart() {
        double Z1 = 0;
        double Z2 = 0;
        double Z3 = 0;
        double Z4 = 0;
        double Z5 = 0;

        for (int i = 0; i < this.trackdata.getNumPoints(); i = i + 3) {
            double HRpoint = this.trackdata.getChunks().get(i).getAvgHeartRate();
            if (HRpoint > 0.9 * this.trackdata.getMaxHeartrate()) {
                Z5++;
            } else if (HRpoint > 0.8 * this.trackdata.getMaxHeartrate()) {
                Z4++;
            } else if (HRpoint > 0.7 * this.trackdata.getMaxHeartrate()) {
                Z3++;
            } else if (HRpoint > 0.6 * this.trackdata.getMaxHeartrate()) {
                Z2++;
            } else {
                Z1++;
            }
        }

        Double totalHR = Z1 + Z2 + Z3 + Z4 + Z5;
        ObservableList<PieChart.Data> pieChartData
                = FXCollections.observableArrayList(
                new PieChart.Data("Z1 Recuperación", 100 * (Z1 / totalHR)),
                new PieChart.Data("Z2 Fondo", 100 * (Z2 / totalHR)),
                new PieChart.Data("Z3 Tempo", 100 * (Z3 / totalHR)),
                new PieChart.Data("Z4 Umbral", 100 * (Z4 / totalHR)),
                new PieChart.Data("Z5 Anaeróbico", 100 * (Z5 / totalHR)));
        piechart.setData(pieChartData);
    }

    private void setMultipleLineChart() {
        yAxis2.autosize();
        xAxis2.autosize();
        XYChart.Series statsProfile1 = new XYChart.Series();
        statsProfile1.setName("Velocidad");
        XYChart.Series statsProfile2 = new XYChart.Series();
        statsProfile2.setName("Frecuencia cardíaca");
        XYChart.Series statsProfile3 = new XYChart.Series();
        statsProfile3.setName("Cadencia Media");
        linechart.getData().clear();
        linechart.setLegendVisible(true);

        if (isDistance) {
            for (int lineChartValue = 1; lineChartValue < 4; lineChartValue++) {
                for (int i = 0; i < this.trackdata.getNumPoints(); i = i + this.chartSplit) {
                    double distancePoint = Worker.convertMetresToKM(i * (this.trackdata.getTotalDistance() / this.trackdata.getNumPoints()));
                    if (lineChartValue == 1) {
                        statsProfile1.getData().add(new XYChart.Data(distancePoint, Worker.convertToKMH(this.trackdata.getChunks().get(i).getSpeed())));
                    } else if (lineChartValue == 2) {
                        statsProfile2.getData().add(new XYChart.Data(distancePoint, this.trackdata.getChunks().get(i).getAvgHeartRate()));
                    } else if (lineChartValue == 3) {
                        statsProfile3.getData().add(new XYChart.Data(distancePoint, this.trackdata.getChunks().get(i).getAvgCadence()));
                    }
                }
            }
            xAxis2.setLabel("Distancia (km)");
        } else {
            for (int lineChartValue = 1; lineChartValue < 4; lineChartValue++) {
                for (int i = 0; i < this.trackdata.getNumPoints(); i = i + this.chartSplit) {
                    double timePoint = i * (this.trackdata.getTotalDuration().toMinutes() / (double) this.trackdata.getNumPoints());
                    if (lineChartValue == 1) {
                        statsProfile1.getData().add(new XYChart.Data(timePoint, Worker.convertToKMH(this.trackdata.getChunks().get(i).getSpeed())));
                    } else if (lineChartValue == 2) {
                        statsProfile2.getData().add(new XYChart.Data(timePoint, this.trackdata.getChunks().get(i).getAvgHeartRate()));
                    } else if (lineChartValue == 3) {
                        statsProfile3.getData().add(new XYChart.Data(timePoint, this.trackdata.getChunks().get(i).getAvgCadence()));
                    }
                }
            }
            xAxis2.setLabel("Tiempo (min)");
        }

        this.velocidadSerie = statsProfile1;
        this.frecuenciaSerie = statsProfile2;
        this.cadenciaSerie = statsProfile3;

        linechart.getData().addAll(statsProfile1, statsProfile2, statsProfile3);
    }

    @FXML
    private void setComboBox() {
        String comboValue = combobox.getSelectionModel().getSelectedItem().toString();

        switch (comboValue) {
            case "Altura x Distancia":
                areachart.setVisible(true);
                linechart.setVisible(false);
                piechart.setVisible(false);
                yAxis.setLabel("Altura");
                radDist.setDisable(false);
                radTime.setDisable(false);
                checkV.setVisible(false);
                checkF.setVisible(false);
                checkC.setVisible(false);
                setAreaChart();
                break;

            case "Velocidad x Distancia":
                areachart.setVisible(false);
                linechart.setVisible(true);
                piechart.setVisible(false);
                yAxis2.setLabel("Velocidad");
                radDist.setDisable(false);
                radTime.setDisable(false);
                checkV.setVisible(false);
                checkF.setVisible(false);
                checkC.setVisible(false);
                setLineChart(1);
                break;

            case "FC x Distancia":
                areachart.setVisible(false);
                linechart.setVisible(true);
                piechart.setVisible(false);
                yAxis2.setLabel("Frecuencia Cardíaca");
                radDist.setDisable(false);
                radTime.setDisable(false);
                checkV.setVisible(false);
                checkF.setVisible(false);
                checkC.setVisible(false);
                setLineChart(2);
                break;

            case "Cadencia x Distancia":
                areachart.setVisible(false);
                linechart.setVisible(true);
                piechart.setVisible(false);
                yAxis2.setLabel("Cadencia");
                radDist.setDisable(false);
                radTime.setDisable(false);
                checkV.setVisible(false);
                checkF.setVisible(false);
                checkC.setVisible(false);
                setLineChart(3);
                break;

            case "Esfuerzo cardíaco":
                areachart.setVisible(false);
                linechart.setVisible(false);
                piechart.setVisible(true);
                radDist.setDisable(true);
                radTime.setDisable(true);
                checkV.setVisible(false);
                checkF.setVisible(false);
                checkC.setVisible(false);
                setPieChart();
                break;

            case "Combinación":
                areachart.setVisible(false);
                linechart.setVisible(true);
                piechart.setVisible(false);
                // yAxis2.setLabel("Cadencia");
                radDist.setDisable(false);
                radTime.setDisable(false);
                checkV.setVisible(true);
                checkF.setVisible(true);
                checkC.setVisible(true);
                setMultipleLineChart();
                break;
        }
    }
}
