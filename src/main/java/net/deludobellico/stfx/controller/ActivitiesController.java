/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.deludobellico.stfx.controller;

import net.deludobellico.stfx.STFXApp;
import net.deludobellico.stfx.model.manager.TrackView;
import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javax.xml.bind.JAXBException;

/**
 * FXML Controller class
 *
 * @author Mario Gomez
 */
public class ActivitiesController implements Initializable {

    @FXML
    private TableView<TrackView> tableview;

    @FXML
    private TableColumn name;

    @FXML
    private TableColumn date;

    @FXML
    private TableColumn distance;

    @FXML
    private TableColumn duration;

    @FXML
    private TableColumn ritm;

    @FXML
    private Label yearActivitiesLbl;

    @FXML
    private Label monthLabel1;

    @FXML
    private Label monthLabel2;

    @FXML
    private Label monthLabel3;

    @FXML
    private Label monthLabel4;

    @FXML
    private Label monthLabel5;

    @FXML
    private Label distanceLabel1;

    @FXML
    private Label distanceLabel2;

    @FXML
    private Label distanceLabel3;

    @FXML
    private Label durationLabel1;

    @FXML
    private Label durationLabel2;

    @FXML
    private Label durationLabel3;

    @FXML
    private Button showActivity;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            Calendar c = Calendar.getInstance();
            int Month = c.get(Calendar.MONTH);
            int Year = c.get(Calendar.YEAR);

            // Actividades por mes del año actual
            yearActivitiesLbl.setText(STFXApp.trackManager.getTracksCountByYear(Year) + "");
            monthLabel1.setText(STFXApp.trackManager.getTracksCountByMonth(1, Year) + "");
            monthLabel2.setText(STFXApp.trackManager.getTracksCountByMonth(2, Year) + "");
            monthLabel3.setText(STFXApp.trackManager.getTracksCountByMonth(3, Year) + "");
            monthLabel4.setText(STFXApp.trackManager.getTracksCountByMonth(4, Year) + "");
            monthLabel5.setText(STFXApp.trackManager.getTracksCountByMonth(5, Year) + "");

            // Actividades por distancia
            distanceLabel1.setText(STFXApp.trackManager.getTracksCountByDistance(0, 49) + "");
            distanceLabel2.setText(STFXApp.trackManager.getTracksCountByDistance(50, 100) + "");
            distanceLabel3.setText(STFXApp.trackManager.getTracksCountByDistance(101, 1000) + "");

            // Actividades por duración
            durationLabel1.setText(STFXApp.trackManager.getTracksCountByDuration(0, 59) + "");
            durationLabel2.setText(STFXApp.trackManager.getTracksCountByDuration(60, 120) + "");
            durationLabel3.setText(STFXApp.trackManager.getTracksCountByDuration(121, 3600) + "");

            updateTable(STFXApp.trackManager.getTrackViewFromYear(Year));

            // Botón Activado:
            showActivity.disableProperty().bind(Bindings.equal(-1, tableview.getSelectionModel().selectedIndexProperty()));

            // GpsVisualizer.getMap();
        } catch (JAXBException ex) {
        }
    }

    private void updateTable(List<TrackView> tracks) {
        tableview.getItems().clear();

        ObservableList<TrackView> componentes = FXCollections.observableArrayList(tracks);
        name.setCellValueFactory(new PropertyValueFactory<>("name"));
        date.setCellValueFactory(new PropertyValueFactory<>("date"));
        distance.setCellValueFactory(new PropertyValueFactory<>("distance"));
        duration.setCellValueFactory(new PropertyValueFactory<>("duration"));
        ritm.setCellValueFactory(new PropertyValueFactory<>("speed"));
        tableview.setItems(componentes);
    }

    @FXML
    private void showMonthActivities(MouseEvent e) {
        String paneName = ((Node) e.getSource()).getId();
        int Month = Integer.parseInt(paneName.split("_")[1]);
        List<TrackView> tracks;
        if (Month == 0) {
            tracks = STFXApp.trackManager.getTrackViewFromYear(2016);
        } else {
            tracks = STFXApp.trackManager.getTrackViewFromMonth(Month, 2016);
        }
        updateTable(tracks);
    }

    @FXML
    private void showDistanceActivities(MouseEvent e) {
        String paneName = ((Node) e.getSource()).getId();
        int minDistance = Integer.parseInt(paneName.split("_")[1]);
        int maxDistance = Integer.parseInt(paneName.split("_")[2]);
        List<TrackView> tracks = STFXApp.trackManager.getTrackViewFromDistance(minDistance, maxDistance);
        updateTable(tracks);
    }

    @FXML
    private void showDurationActivities(MouseEvent e) {
        String paneName = ((Node) e.getSource()).getId();
        int minDuration = Integer.parseInt(paneName.split("_")[1]);
        int maxDuration = Integer.parseInt(paneName.split("_")[2]);
        List<TrackView> tracks = STFXApp.trackManager.getTrackViewFromDuration(minDuration, maxDuration);
        updateTable(tracks);
    }

    @FXML
    private void showActivityMethod() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/ActivityView.fxml"));
            Parent root = fxmlLoader.load();
            ActivityController activityController = fxmlLoader.getController();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();
            activityController.showActivity(tableview.getSelectionModel().getSelectedItem(), null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
