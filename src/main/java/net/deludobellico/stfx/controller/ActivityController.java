package net.deludobellico.stfx.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import net.deludobellico.stfx.STFXApp;
import net.deludobellico.stfx.analysis.TrackData;
import net.deludobellico.stfx.model.manager.TrackView;
import net.deludobellico.stfx.util.Worker;
import net.deludobellico.stfx.util.DateTimeUtils;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author Mario Gomez
 */
public class ActivityController implements Initializable {

    @FXML
    private Label startTime;

    @FXML
    private Label duration;

    @FXML
    private Label movingTime;

    @FXML
    private Label distance;

    @FXML
    private Label ascent;

    @FXML
    private Label descend;

    @FXML
    private Label averageSpeed;

    @FXML
    private Label maxSpeed;

    @FXML
    private Label maxHeartRate;

    @FXML
    private Label minHeartRate;

    @FXML
    private Label averageHeartRate;

    @FXML
    private Label maxCadence;

    @FXML
    private Label averageCadence;

    @FXML
    private MapController mapController;

    private TrackView trackView;
    private TrackData trackData;



    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    public void showActivity(TrackView trackView, TrackData trackData) {
        this.trackView = trackView;
        if (trackData == null) {
            this.trackData = STFXApp.trackManager.getTracks().get(this.trackView.getName());
        } else {
            this.trackData = trackData;
        }

        // Cargamos los datos en los Labels
        this.startTime.setText(DateTimeUtils.format(this.trackData.getStartTime()));
        this.duration.setText(DateTimeUtils.format(this.trackData.getTotalDuration()));
        this.movingTime.setText(DateTimeUtils.format(this.trackData.getMovingTime()));
        this.distance.setText(String.format("%.0f km", Worker.convertMetresToKM(this.trackData.getTotalDistance())));
        this.ascent.setText(String.format("Subida: %.2f m", this.trackData.getTotalAscent()));
        this.descend.setText(String.format("Bajada: %.2f m", this.trackData.getTotalDescend()));
        this.averageSpeed.setText(String.format("Media: %.2f km/h", Worker.convertToKMH(this.trackData.getAverageSpeed())));
        this.maxSpeed.setText(String.format("Máxima: %.2f km/h", Worker.convertToKMH(this.trackData.getMaxSpeed())));
        this.maxHeartRate.setText(String.format("Máxima: %d bpm", this.trackData.getMaxHeartrate()));
        this.minHeartRate.setText(String.format("Mínima: %d bpm", this.trackData.getMinHeartRate()));
        this.averageHeartRate.setText(String.format("%d bpm", this.trackData.getAverageHeartrate()));
        this.maxCadence.setText(String.format("Máxima: %d p/m", this.trackData.getMaxCadence()));
        this.averageCadence.setText(String.format("Media: %d p/m", this.trackData.getAverageCadence()));


        mapController.showTrackOnMap(this.trackData);

//        // Al terminar el hilo oculta esta imagen.
//        loading.visibleProperty().bind(task.runningProperty());
//
//        // Iniciamos el hilo.
//        Thread th = new Thread(task);
//        th.setDaemon(true);
//        th.start();
    }

//    // Hilo: Cuando termina de cargar muestra el mapa.
//    Task<Void> task = new Task<Void>() {
//        @Override
//        protected Void call() throws Exception {
//            Url = GpsVisualizer.getMap(trackView.getName(), isCustom);
//            Platform.runLater(new Runnable() {
//                @Override
//                public void run() {
//                    webView.getEngine().load(Url);
//                }
//            });
//            return null;
//        }
//
//        @Override
//        protected void succeeded() {
//            webView.getEngine().load(Url)
//        }
//    };

    @FXML
    private void showCharts() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/ChartsView.fxml"));
            Parent root = fxmlLoader.load();
            fxmlLoader.<GraphicsDocumentController>getController().loadInformation(this.trackData);
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }


}
