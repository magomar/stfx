package net.deludobellico.stfx.controller;

import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.concurrent.Worker.State;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.util.Duration;
import net.deludobellico.stfx.analysis.Chunk;
import net.deludobellico.stfx.analysis.GPSDistanceFunction;
import net.deludobellico.stfx.analysis.TrackData;
import net.deludobellico.stfx.model.gpx.Waypoint;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Mario on 27/05/2015.
 */
public class MapController implements Initializable {
    @FXML
    private ToggleButton satellite;

    @FXML
    private WebView mapWebView;

    @FXML
    private ToggleButton hybrid;

    @FXML
    private ToggleButton google;

    @FXML
    private ToggleButton yahoo;

    @FXML
    private ToggleButton terrain;

    @FXML
    private ToggleGroup mapTypeGroup;

    @FXML
    private ToggleGroup mapSourceGroup;

    @FXML
    private ToggleButton road;

    @FXML
    private ToggleButton bing;

    @FXML
    private TextField searchBox;

    @FXML
    private Button zoomIn;

    @FXML
    private Button zoomOut;

    @FXML
    private ImageView loadingImage;

    @FXML
    private StackPane mapPane;
    private WebEngine webEngine;
    private Timeline locationUpdateTimeline;

    private TrackData trackData;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        mapTypeGroup.selectedToggleProperty().addListener(
                (observableValue, toggle, toggle1) -> {
                    if (road.isSelected()) {
                        webEngine.executeScript("document.setMapTypeRoad()");
                    } else if (satellite.isSelected()) {
                        webEngine.executeScript("document.setMapTypeSatellite()");
                    } else if (hybrid.isSelected()) {
                        webEngine.executeScript("document.setMapTypeHybrid()");
                    } else if (terrain.isSelected()) {
                        webEngine.executeScript("document.setMapTypeTerrain()");
                    }
                });
        mapSourceGroup.selectedToggleProperty().addListener(
                new ChangeListener<Toggle>() {
                    public void changed(
                            ObservableValue<? extends Toggle> observableValue,
                            Toggle toggle, Toggle toggle1) {
                        terrain.setDisable(true);
                        if (google.isSelected()) {
                            terrain.setDisable(false);
                            webEngine.load(
                                    getClass().getClassLoader().getResource("html/googlemap.html").toString());
                        } else if (yahoo.isSelected()) {
                            webEngine.load(
                                    getClass().getClassLoader().getResource("html/bingmap.html").toString());
                        } else if (bing.isSelected()) {
                            webEngine.load(
                                    getClass().getClassLoader().getResource("html/yahoomap.html").toString());
                        }
                        mapTypeGroup.selectToggle(road);
                    }
                });

        searchBox.textProperty().addListener((observableValue, s, s1) -> {
            // delay location updates to we don't go too fast file typing
            if (locationUpdateTimeline != null) locationUpdateTimeline.stop();
            locationUpdateTimeline = new Timeline();
            locationUpdateTimeline.getKeyFrames().add(
                    new KeyFrame(new Duration(400),
                            actionEvent -> {
                                webEngine.executeScript("document.goToLocation(\"" +
                                        searchBox.getText() + "\")");
                            })
            );
            locationUpdateTimeline.play();
        });

        final URL urlGoogleMaps = getClass().getClassLoader().getResource("html/googlemap.html");
        webEngine = mapWebView.getEngine();
        webEngine.getLoadWorker().stateProperty().addListener(
                (ov, oldState, newState) -> {
                    if (newState == State.SUCCEEDED) {
                        renderMap();
                    }

                });
        webEngine.load(urlGoogleMaps.toExternalForm());
    }

    public void showTrackOnMap(TrackData trackData) {
        this.trackData = trackData;
    }

    @FXML
    void zoomInAction(ActionEvent event) {
        webEngine.executeScript("document.zoomIn()");
    }

    @FXML
    void zoomOutAction(ActionEvent event) {
        webEngine.executeScript("document.zoomOut()");
    }


    private void renderMap() {
        if (trackData==null) return;
//        FadeTransition fadeOut = new FadeTransition(Duration.millis(2000), loadingImage);
//        fadeOut.setFromValue(1.0);
//        fadeOut.setToValue(0.0);
//        FadeTransition fadeIn = new FadeTransition(Duration.millis(2500), mapWebView);
//        fadeIn.setFromValue(0.0);
//        fadeIn.setToValue(1.0);
//        fadeOut.play();
//        fadeIn.play();
        webEngine.executeScript("document.clearTrack()");
        List<Chunk> chunks = trackData.getChunks();
        Waypoint w1 = chunks.get(0).getFirstPoint();
        Waypoint w2 = chunks.get(chunks.size() - 1).getLastPoint();
        Point2D p = GPSDistanceFunction.getMiddlePoint(w1.getLatitude(), w1.getLongitude(),
                w2.getLatitude(), w2.getLongitude());
        webEngine.executeScript("document.goToCoordinates(" + p.getX() + "," + p.getY() + ")");
//        webEngine.executeScript("document.addPoint(" + w1.getLatitude() + "," + w1.getLongitude() + ")");
        for (Chunk chunk : chunks) {
            Waypoint w = chunk.getLastPoint();
            webEngine.executeScript("document.addPoint(" + w.getLatitude() + "," + w.getLongitude() + ")");
        }
        webEngine.executeScript("document.drawTrack()");
        loadingImage.setVisible(false);
        mapWebView.setVisible(true);
    }
}
