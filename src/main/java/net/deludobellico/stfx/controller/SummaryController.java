/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.deludobellico.stfx.controller;

import net.deludobellico.stfx.STFXApp;
import net.deludobellico.stfx.model.manager.TrackView;
import net.deludobellico.stfx.util.Worker;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import net.deludobellico.stfx.analysis.TrackData;
import net.deludobellico.stfx.exception.DriverPropertiesException;
import net.deludobellico.stfx.exception.DriverReaderException;
import net.deludobellico.stfx.exception.DriverValidationException;
import net.deludobellico.stfx.model.gpx.Gpx;
import net.deludobellico.stfx.persistence.driver.DataReader;
import net.deludobellico.stfx.util.DateTimeUtils;

import javax.xml.bind.JAXBException;

/**
 *
 * @author Mario Gomez
 */
public class SummaryController implements Initializable {

    @FXML
    private Label lastDateLabel;

    @FXML
    private Label ciclismoLabel;

    @FXML
    private Label distanciaData;

    @FXML
    private Label durationData;

    @FXML
    private Label vmediaData;

    @FXML
    private Label actualMonthLabel;

    @FXML
    private Label actualMonthActivities;

    @FXML
    private Label actualMonthDistance;

    @FXML
    private Label actualMonthDuration;

    @FXML
    private Label actualMonthSpeed;

    @FXML
    private VBox lastActPane1;

    @FXML
    private VBox lastActPane2;

    @FXML
    private VBox lastActPane3;

    @FXML
    private VBox lastActPane4;

    @FXML
    private BarChart<String, Double> barChart;

    @FXML
    private CategoryAxis xAxis;

    @FXML
    private NumberAxis yAxis;

    @FXML
    private Label totalActivitiesYear;

    @FXML
    private Label totalDistanceYear;

    @FXML
    private Label totalDurationYear;

    @FXML
    private Label totalVMYear;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {

            Calendar c = Calendar.getInstance();

            // LastActivity:
            TrackData lastTrack = STFXApp.trackManager.getLastTrack();
            distanciaData.setText(String.format("%.0f km", Worker.convertMetresToKM(lastTrack.getTotalDistance())));
            durationData.setText(DateTimeUtils.format(lastTrack.getTotalDuration()));
            vmediaData.setText(String.format("%.2f km/h", Worker.convertToKMH(lastTrack.getAverageSpeed())));

            // MonthlyStatistics
            actualMonthLabel.setText("Estadísticas (" + Worker.integerToMonth(c.get(Calendar.MONTH) - 1) + ")");
            List<TrackData> t = STFXApp.trackManager.getMonthlyActivities(c.get(Calendar.MONTH), c.get(Calendar.YEAR));
            actualMonthActivities.setText(t.size() + " Actividades");
            actualMonthDistance.setText(String.format("%.0f km", Worker.convertMetresToKM(Worker.totalDistance(t))));
            actualMonthDuration.setText(Worker.generateTotalDuration(Worker.totalDuration(t)));
            actualMonthSpeed.setText(String.format("%.2f km/h", Worker.convertToKMH(Worker.averageSpeed(t))));

            // YearStatistics
            int yearCount = STFXApp.trackManager.getTracksCountByYear(c.get(Calendar.YEAR));
            totalActivitiesYear.setText(yearCount + "");
            totalDistanceYear.setText(String.format("%.0f km", Worker.convertMetresToKM(STFXApp.trackManager.getTotalDistanceByYear(c.get(Calendar.YEAR)))));
            totalDurationYear.setText(Worker.generateTotalDuration(STFXApp.trackManager.getTotalDurationByYear(c.get(Calendar.YEAR))));
            totalVMYear.setText(String.format("%.2f km/h", Worker.convertToKMH(STFXApp.trackManager.getTotalVMByYear(c.get(Calendar.YEAR)) / yearCount)));

            // BarChart
            updateBarChart(1);

        } catch (JAXBException ex) {
        }
    }

    private void updateBarChart(int Type) throws JAXBException {
        Calendar c = Calendar.getInstance();
        barChart.getData().clear();

        XYChart.Series Meses = new XYChart.Series();
        // Meses.setName("Kilómetros");
        for (int i = 1; i < c.get(Calendar.MONTH) + 2; i++) {
            generateChartMonth(Meses, i, c.get(Calendar.YEAR), Type);
        }
        barChart.getData().addAll(Meses);
    }

    private void generateChartMonth(XYChart.Series Meses, int Month, int Year, int Type) throws JAXBException {
        if (Type == 1) {
            // Contar Actividades
            yAxis.setLabel("Actividades");
            Meses.getData().add(new XYChart.Data(Worker.integerToMonth(Month - 1), STFXApp.trackManager.getTracksCountByMonth(Month, Year)));
        } else if (Type == 2) {
            // Contar Distancias
            yAxis.setLabel("Kilómetros");
            Meses.getData().add(new XYChart.Data(Worker.integerToMonth(Month - 1), Worker.convertMetresToKM(STFXApp.trackManager.getTracksDistanceByMonth(Month, Year))));
        } else if (Type == 3) {
            // Contar Duración
            yAxis.setLabel("Horas");
            Meses.getData().add(new XYChart.Data(Worker.integerToMonth(Month - 1), STFXApp.trackManager.getTracksDurationByMonth(Month, Year)));
        } else if (Type == 4) {
            // Contar Duración
            yAxis.setLabel("KM/H");
            Meses.getData().add(new XYChart.Data(Worker.integerToMonth(Month - 1), Worker.convertToKMH(STFXApp.trackManager.getTracksVMByMonth(Month, Year))));
        }
    }

    @FXML
    private void lastWeekActivitiesHover(MouseEvent e) throws JAXBException {
        // Eliminamos el elemento actual tintado de verde.
        lastActPane1.getStyleClass().remove("greenbg");
        lastActPane2.getStyleClass().remove("greenbg");
        lastActPane3.getStyleClass().remove("greenbg");
        lastActPane4.getStyleClass().remove("greenbg");

        // obtenemos el nombre del elemento sobre el que está el ratón.
        String paneName = ((Node) e.getSource()).getId();

        // Eliminamos el elemento de CSS whitebg y a continuación añadimos el verde greenbg
        switch (paneName) {
            case "lastActPane1":
                lastActPane1.getStyleClass().remove("whitebg");
                lastActPane1.getStyleClass().add("greenbg");
                updateBarChart(1);
                break;

            case "lastActPane2":
                lastActPane2.getStyleClass().remove("whitebg");
                lastActPane2.getStyleClass().add("greenbg");
                updateBarChart(2);
                break;

            case "lastActPane3":
                lastActPane3.getStyleClass().remove("whitebg");
                lastActPane3.getStyleClass().add("greenbg");
                updateBarChart(3);
                break;

            case "lastActPane4":
                lastActPane4.getStyleClass().remove("whitebg");
                lastActPane4.getStyleClass().add("greenbg");
                updateBarChart(4);
                break;
        }
    }

    @FXML
    private void openActivitiesView() {
        try {
            FXMLLoader miCargador = new FXMLLoader(getClass().getResource("/view/ActivitiesView.fxml"));
            Parent root = (Parent) miCargador.load();
            Scene scene = new Scene(root);
            Stage estageActual = new Stage();
            estageActual.setScene(scene);
            estageActual.show();
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }

    @FXML
    private void loadCustomTrack() throws JAXBException, IOException, DriverReaderException, DriverValidationException, DriverPropertiesException {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("GPX files (*.gpx)", "*.gpx");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        Stage stage = (Stage) distanciaData.getScene().getWindow();
        File file = fileChooser.showOpenDialog(stage);

        if (file != null) {
            DataReader reader = STFXApp.trackManager.getReader();
            Gpx gpxType = reader.readGpxDocument(file);
            TrackData t = new TrackData(gpxType.getTracks().get(0));
            TrackView tv = new TrackView(file.getName(), t.getTotalDistance(), t.getTotalDuration().toMinutes(), t.getAverageSpeed());
            // TODO change this approach
            Worker.copyCustomTrack(file);
            showActivityMethod(tv, t);
        }
    }

    private void showActivityMethod(TrackView track, TrackData t) {
        try {
            FXMLLoader miCargador = new FXMLLoader(getClass().getResource("/view/ActivityView.fxml"));
            Parent root = (Parent) miCargador.load();
            miCargador.<ActivityController>getController().showActivity(track, t);
            Scene scene = new Scene(root);
            Stage estageActual = new Stage();
            estageActual.setScene(scene);
            estageActual.show();
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }
}
