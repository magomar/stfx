package net.deludobellico.stfx.analysis;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import net.deludobellico.stfx.model.gpx.Track;
import net.deludobellico.stfx.util.DateTimeUtils;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * This class holds all the information obtained from a complete activity recorded with a GPS, and
 * represented by an instance  of the {@link Track} class. This information is obtained from the basic data included in the source file of the track (a GPX file), plus a lot of
 * derived information, such as distance, speed, elevation gain, maximun, minimun and average values.
 */
public class TrackData {

    /**
     * Date and time when the activity was initiated
     */
    private final ObjectProperty<LocalDateTime> startTime = new SimpleObjectProperty<>();
    /**
     * Date and time when the activity ended
     */
    private final ObjectProperty<LocalDateTime> endTime = new SimpleObjectProperty<>();
    /**
     * Total time passed between the start and the end of the activity
     */
    private final ObjectProperty<Duration> totalDuration = new SimpleObjectProperty<>();
    /**
     * Total distance traversed during the realization of the activity
     */
    private final DoubleProperty totalDistance = new SimpleDoubleProperty();
    /**
     * Accumulated increment of altitud that occurred along the entire activity
     */
    private final DoubleProperty totalAscent = new SimpleDoubleProperty();
    /**
     * Accumulated decrement of altitud that occurred along the entire activity
     */
    private final DoubleProperty totalDescend = new SimpleDoubleProperty();
    /**
     * Average elevation at which the activity took place
     */
    private final IntegerProperty averageHeight = new SimpleIntegerProperty();
    /**
     * Minimum elevation reached during the activity
     */
    private final IntegerProperty minHeight = new SimpleIntegerProperty();
    /**
     * Maximum elevation reached during the activity
     */
    private final IntegerProperty maxHeight = new SimpleIntegerProperty();
    /**
     * Average speed for the entire activity, specified in meters per second
     */
    private final DoubleProperty averageSpeed = new SimpleDoubleProperty();
    /**
     * Maximum speed reached during the activity
     */
    private final DoubleProperty maxSpeed = new SimpleDoubleProperty();
    /**
     * Total moving time accumulated during the entire activity. Moving means
     * that the speed is close to 0, which in practice means the speed is lower
     * than certain threshold value.
     */
    private final ObjectProperty<Duration> movingTime = new SimpleObjectProperty<>();
    /**
     * Average pace at which the activity was performed. In ciclyng this value
     * refers to the frequency of pedal strokes per minute. In running/walking
     * activities cadence refers to the frequency of steps per minute.
     */
    private final IntegerProperty averageCadence = new SimpleIntegerProperty();
    /**
     * Maximum cadence achieved during the activity
     */
    private final IntegerProperty maxCadence = new SimpleIntegerProperty();
    /**
     * Average heart rate, specified as beats per minute (bpm)
     */
    private final IntegerProperty averageHeartrate = new SimpleIntegerProperty();
    /**
     * Minimum heart rate reached during the activity
     */
    private final IntegerProperty minHeartRate = new SimpleIntegerProperty();
    /**
     * Maximum heart rate reached during the activity
     */
    private final IntegerProperty maxHeartrate = new SimpleIntegerProperty();
    /**
     * Threshold used to decide whether there is real movement
     */
    private final DoubleProperty speedThreshold = new SimpleDoubleProperty();

    /**
     * Total number of waypoints in the activity
     */
    private final IntegerProperty numPoints = new SimpleIntegerProperty();

    private final ObservableList<Chunk> chunks = FXCollections.observableArrayList();

    public TrackData(Track track) {
        double maxSpeed = Double.MIN_VALUE; // in m/s
        double avgHeight = 0; // in meters
        double minHeight = Double.MAX_VALUE;
        double maxHeight = Double.MIN_VALUE;
        double avgCadence = 0; // pedalying frecuency (per minute)
        double maxCad = Double.MIN_VALUE;
        double maxHeartRate = Double.MIN_VALUE; // heart beats per minute
        double minHr = Double.MAX_VALUE;
        double avgHearRate = 0;

        Chunk trackChunk = new Chunk(track);
        chunks.addAll(trackChunk.getChunks());
        for (Chunk chunk : chunks) {
            double elevation = chunk.getLastPoint().getElevation(); //in meters
            double speed = chunk.getSpeed(); //in Km/h meters per second
            if (speed > maxSpeed) maxSpeed = speed;
            if (elevation > maxHeight) maxHeight = elevation;
            else if (elevation < minHeight) minHeight = elevation;
            avgHeight += chunk.getAvgHeight() * chunk.getDistance();
            avgCadence += chunk.getAvgCadence() * chunk.getDistance();
            avgHearRate += chunk.getAvgHeartRate() * chunk.getDistance();
            if (chunk.getAvgCadence() > maxCad) maxCad = chunk.getAvgCadence();
            if (chunk.getAvgHeartRate() > maxHeartRate) maxHeartRate = chunk.getAvgHeartRate();
            else if (chunk.getAvgHeartRate() < minHr && chunk.getAvgHeartRate() > TrackAnalysisSettings.DEFAULT_HEART_RATE_THRESHOLD)
                minHr = chunk.getAvgHeartRate();
        }

        startTime.set(trackChunk.getFirstPoint().getTime());
        endTime.set(trackChunk.getLastPoint().getTime());
        totalDuration.set(trackChunk.getDuration());
        movingTime.set(trackChunk.getMovingTime());
        totalDistance.set(trackChunk.getDistance());
        totalAscent.set(trackChunk.getAscent());
        totalDescend.set(trackChunk.getDescend());
        averageHeight.set((int) (avgHeight / trackChunk.getDistance()));
        this.minHeight.set((int) minHeight);
        this.maxHeight.set((int) maxHeight);
        averageCadence.set((int) (avgCadence / trackChunk.getDistance()));
        maxCadence.set((int) maxCad);
        averageHeartrate.set((int) (avgHearRate / trackChunk.getDistance()));
        if (minHr < 250.0) minHeartRate.set((int) minHr);
        this.maxHeartrate.set((int) maxHeartRate);
        averageSpeed.set(trackChunk.getDistance() / trackChunk.getMovingTime().getSeconds());
        this.maxSpeed.set(maxSpeed);
        numPoints.set(trackChunk.getChunks().size());
//        System.out.println(this.toString());

    }

    /***
     * Gets the date and time when the activity was initiated
     *
     * @return
     */
    public LocalDateTime getStartTime() {
        return startTime.get();
    }

    public ObjectProperty<LocalDateTime> startTimeProperty() {
        return startTime;
    }

    /**
     * Gets the date and time when the activity ended
     *
     * @return
     */
    public LocalDateTime getEndTime() {
        return endTime.get();
    }

    public ObjectProperty<LocalDateTime> endTimeProperty() {
        return endTime;
    }

    /**
     * Gets the total duration of the activity, from the {@link #startTime} to the {@link #endTime}
     *
     * @return
     */
    public Duration getTotalDuration() {
        return totalDuration.get();
    }

    public ObjectProperty<Duration> totalDurationProperty() {
        return totalDuration;
    }

    /**
     * Gets the total distance traversed during the realization of the activity
     *
     * @return
     */
    public double getTotalDistance() {
        return totalDistance.get();
    }

    public DoubleProperty totalDistanceProperty() {
        return totalDistance;
    }

    /**
     * Gets the accumulated increment of altitud that occurred along the entire activity
     *
     * @return
     */
    public double getTotalAscent() {
        return totalAscent.get();
    }

    public DoubleProperty totalAscentProperty() {
        return totalAscent;
    }

    /**
     * Gets the accumulated decrement of altitud that occurred along the entire activity
     *
     * @return
     */
    public double getTotalDescend() {
        return totalDescend.get();
    }

    public DoubleProperty totalDescendProperty() {
        return totalDescend;
    }

    /**
     * Gets the average elevation at which the activity took place
     *
     * @return
     */
    public int getAverageHeight() {
        return averageHeight.get();
    }

    public IntegerProperty averageHeightProperty() {
        return averageHeight;
    }

    /**
     * Gets the minimum elevation reached during the activity
     *
     * @return
     */
    public int getMinHeight() {
        return minHeight.get();
    }

    public IntegerProperty minHeightProperty() {
        return minHeight;
    }

    /**
     * Gests the maximum elevation reached during the activity
     *
     * @return
     */
    public int getMaxHeight() {
        return maxHeight.get();
    }

    public IntegerProperty maxHeightProperty() {
        return maxHeight;
    }

    /**
     * Gets the average speed for the entire activity
     *
     * @return
     */
    public double getAverageSpeed() {
        return averageSpeed.get();
    }

    public DoubleProperty averageSpeedProperty() {
        return averageSpeed;
    }

    /**
     * Gets the maximum speed reached during the activity
     *
     * @return
     */
    public double getMaxSpeed() {
        return maxSpeed.get();
    }

    public DoubleProperty maxSpeedProperty() {
        return maxSpeed;
    }

    /**
     * Gets the total moving time accumulated during the entire activity. Moving means that the
     * speed is close to 0, which in practice means the speed is lower than certain threshold value.
     *
     * @return
     */
    public Duration getMovingTime() {
        return movingTime.get();
    }

    public ObjectProperty<Duration> movingTimeProperty() {
        return movingTime;
    }

    /**
     * Gets the average cadence for the entire activity.
     *
     * @return
     */
    public int getAverageCadence() {
        return averageCadence.get();
    }

    public IntegerProperty averageCadenceProperty() {
        return averageCadence;
    }

    /**
     * Gets the maximum cadence achieved during the activity
     *
     * @return
     */
    public int getMaxCadence() {
        return maxCadence.get();
    }

    public IntegerProperty maxCadenceProperty() {
        return maxCadence;
    }

    /**
     * Gets the average frequency of heart beats per minute
     *
     * @return
     */
    public int getAverageHeartrate() {
        return averageHeartrate.get();
    }

    public IntegerProperty averageHeartrateProperty() {
        return averageHeartrate;
    }

    /**
     * Gets the minimum heart rate achieved during the activity
     *
     * @return
     */
    public int getMinHeartRate() {
        return minHeartRate.get();
    }

    public IntegerProperty minHeartRateProperty() {
        return minHeartRate;
    }

    public ObservableList<Chunk> getChunks() {
        return chunks;
    }

    /**
     * Gets the maximum heart rate achieved during the activity
     *
     * @return
     */
    public int getMaxHeartrate() {
        return maxHeartrate.get();
    }

    public IntegerProperty maxHeartrateProperty() {
        return maxHeartrate;
    }

    public int getNumPoints() {
        return numPoints.get();
    }

    public IntegerProperty numPointsProperty() {
        return numPoints;
    }

//    public ObservableList<XYChart.Series<Number, Integer>> getElevationPerDuration() {
//        return elevationPerDuration;
//    }
//
//    public ObservableList<XYChart.Series<Number, Integer>> getElevationPerDistance() {
//        return elevationPerDistance;
//    }
//
//
//    public ObservableList<XYChart.Series<Number, Double>> getSpeedPerDuration() {
//        return speedPerDuration;
//    }
//
//    public ObservableList<XYChart.Series<Number, Double>> getSpeedPerDistance() {
//        return speedPerDistance;
//    }

    @Override
    public String toString() {
        if (null != startTime.get()) {
            return "TrackData{"
                    + "\n\tstartTime=" + DateTimeUtils.format(startTime.get())
                    + "\n\tendTime=" + DateTimeUtils.format(endTime.get())
                    + String.format("\n\ttotalDistance=%.2f", totalDistance.get())
                    + "\n\ttotalDuration=" + DateTimeUtils.format(totalDuration.get())
                    + String.format("\n\taverageSpeed=%.2f", averageSpeed.get())
                    + String.format("\n\ttotalAscent=%.2f", totalAscent.get())
                    + String.format("\n\ttotalDescend=%.2f", totalDescend.get())
                    + '}';
        } else {
            return "TrackData{"
                    + String.format("\n\ttotalDistance=%.2f", totalDistance.get())
                    + String.format("\n\ttotalAscent=%.2f", totalAscent.get())
                    + String.format("\n\ttotalDescend=%.2f", totalDescend.get())
                    + '}';
        }
    }

//    @Override
//    public String toString() {
//        if (null!= startTime.get())
//            return "TrackData{" +
//                    "startTime=" + DateTimeUtils.format(startTime.get()) +
//                    ", endTime=" + DateTimeUtils.format(endTime.get()) +
//                    String.format(", totalDistance=%.2f", totalDistance.get()) +
//                    ", totalDuration=" + DateTimeUtils.format(totalDuration.get()) +
//                    String.format(", averageSpeed=%.2f", averageSpeed.get()) +
//                    String.format(", totalAscent=%.2f", totalAscent.get()) +
//                    String.format(", totalDescend=%.2f", totalDescend.get()) +
//                    '}';
//        else         return "TrackData{" +
//                String.format("totalDistance=%.2f", totalDistance.get()) +
//                String.format(", totalAscent=%.2f", totalAscent.get()) +
//                String.format(", totalDescend=%.2f", totalDescend.get()) +
//                '}';
//    }
}
