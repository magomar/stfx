package net.deludobellico.stfx.analysis;

import javafx.geometry.Point2D;

import java.math.BigDecimal;

/**
 *
 * Basic implementation of a {@link DistanceFunction} between two geographical points
 * using the <a href="https://en.wikipedia.org/wiki/Haversine_formula">Haversine formula</a>.
 */
public enum GPSDistanceFunction implements DistanceFunction {
    HAVERSINE {
        @Override
        public double getDistance(double latitude1, double longitude1, double latitude2, double longitude2) {
            final double R = 6371000; // Earth radius in metres
            double latDistance = toRadians(latitude2 - latitude1);
            double lonDistance = toRadians(longitude2 - longitude1);
            double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) +
                    Math.cos(toRadians(latitude1)) * Math.cos(toRadians(latitude2)) *
                            Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            return R * c;
        }
    };


    private static double toRadians(double degrees) {
        return degrees * Math.PI / 180;
    }

    private static double toDegrees(double radians) {
        return radians * 180 / Math.PI;
    }

    /**
     * Gets the middle point between two geographical points.
     * @param lat1
     * @param lng1
     * @param lat2
     * @param lng2
     * @return
     */
    public static Point2D getMiddlePoint(double lat1, double lng1, double lat2, double lng2) {
        double lat1r = toRadians(lat1);
        double lng1r = toRadians(lng1);
        double lat2r = toRadians(lat2);
        double lng2r = toRadians(lng2);
        double x1 = Math.cos(lat1r) * Math.cos(lng1r);
        double y1 = Math.cos(lat1r) * Math.sin(lng1r);
        double z1 = Math.sin(lat1r);
        double x2 = Math.cos(lat2r) * Math.cos(lng2r);
        double y2 = Math.cos(lat2r) * Math.sin(lng2r);
        double z2 = Math.sin(lat2r);
        double x = (x1 + x2) / 2;
        double y = (y1 + y2) / 2;
        double z = (z1 + z2) / 2;
        double longitude = Math.atan2(y, x);
        double latitude = Math.atan2(z, Math.sqrt(x*x + y*y));
        Point2D point2D = new Point2D(toDegrees(latitude), toDegrees(longitude));
        return point2D;
    }
}
