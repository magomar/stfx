package net.deludobellico.stfx.analysis;


import net.deludobellico.stfx.model.gpx.Track;
import net.deludobellico.stfx.model.gpx.TrackSegment;
import net.deludobellico.stfx.model.gpx.Waypoint;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * A chunk contains information about two consecutive waypoints
 */
public class Itinerary implements Pathway {
    private double distance; //meters
    private double speed; // meters/second
    private Duration duration;
    private Duration movingTime;
    private double grade; // elevation / distance, expressed as a percent
    private double ascend; // absolute positive increment of height
    private double descend; // absolute negative increment of height
    private Waypoint start;
    private Waypoint end;
    private List<Step> steps = new ArrayList<>();
    private double height;
    private double cadence;
    private double heartRate;
    private GPSDistanceFunction distanceFunction = TrackAnalysisSettings.DEFAULT_DISTANCE_FUNCTION;
    private double speedThreshold = TrackAnalysisSettings.DEFAULT_SPEED_THRESHOLD;
    private double distanceThreshold = TrackAnalysisSettings.DEFAULT_DISTANCE_THRESHOLD;



    public Itinerary(TrackSegment trackSegment) {
        distance = 0;
        duration = Duration.ofSeconds(0);
        movingTime = Duration.ofSeconds(0);
        ascend = 0;
        descend = 0;
        List<Waypoint> waypoints = new ArrayList<>(trackSegment.getWaypoints());
        start = waypoints.get(0);
        end = waypoints.get(waypoints.size());
        Waypoint previous = start;
        steps.add(new Step(start));
    }

    public Itinerary(Track track) {
        distance = 0;
        duration = Duration.ofSeconds(0);
        movingTime = Duration.ofSeconds(0);
        ascend = 0;
        descend = 0;
        start = track.getStart();
        end = track.getEnd();
        Waypoint previous = start;
        steps.add(new Step(start));
        for (TrackSegment trackSegment : track.getTrackSegments())
            for (Waypoint waypoint : trackSegment.getWaypoints()) {
                if (!waypoint.equals(start)) {
                    Step step = new Step(previous, waypoint);
                    previous = waypoint;
                    steps.add(step);
                    duration = duration.plus(step.getDuration());
                    movingTime = movingTime.plus(step.getMovingTime());
                    distance += step.getDistance();
                    if (step.getAscent() >= 0) {
                        ascend += step.getAscent();
                    } else {
                        descend += Math.abs(step.getAscent());
                    }
                }
            }
        speed = distance / getDuration().getSeconds();
    }

    @Override
    public double getDistance() {
        return distance;
    }

    @Override
    public double getSpeed() {
        return speed;
    }

    @Override
    public Duration getDuration() {
        return duration;
    }

    @Override
    public Duration getMovingTime() {
        return movingTime;
    }

    @Override
    public double getGrade() {
        return grade;
    }

    @Override
    public double getAscent() {
        return ascend;
    }

    @Override
    public double getDescend() {
        return descend;
    }

    @Override
    public Waypoint getStart() {
        return start;
    }

    @Override
    public Waypoint getEnd() {
        return end;
    }

    public List<Step> getSteps() {
        return steps;
    }

    public double getElevation() {
        return height;
    }

    public double getCadence() {
        return cadence;
    }

    public double getHeartRate() {
        return heartRate;
    }

    public GPSDistanceFunction getDistanceFunction() {
        return distanceFunction;
    }

    public double getSpeedThreshold() {
        return speedThreshold;
    }

    public double getDistanceThreshold() {
        return distanceThreshold;
    }
}
