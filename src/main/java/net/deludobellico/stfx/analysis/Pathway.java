package net.deludobellico.stfx.analysis;


import net.deludobellico.stfx.model.gpx.Waypoint;

import java.time.Duration;

/**
 *
 */
 interface Pathway {
     double getDistance();

     double getSpeed();

     Duration getDuration();

     double getGrade();

     double getAscent();

     double getDescend();

     Duration getMovingTime();

     double getCadence();

     double getHeartRate();

     double getElevation();

     Waypoint getStart();

     Waypoint getEnd();
}
