package net.deludobellico.stfx.analysis;


import net.deludobellico.stfx.model.gpx.Waypoint;

import java.time.Duration;

/**
 * Contains all the derived information about two points in a track or route:
 * {@link #start} and {@link #end}. It includes many attributes derived from the basic
 * information provided by a GPX, like distance, speed, duration, grade, etc.
 */
public class Step implements Pathway {
    private double distance; //meters
    private double speed; // meters/second
    private Duration duration;
    private Duration movingTime;
    private double grade; // elevation / distance, expressed as a percent
    private double ascent; // absolute positive increment of height
    private double descend; // absolute negative increment of height
    private Waypoint start;
    private Waypoint end;
    private static GPSDistanceFunction distanceFunction = TrackAnalysisSettings.DEFAULT_DISTANCE_FUNCTION;
    private static double speedThreshold = TrackAnalysisSettings.DEFAULT_SPEED_THRESHOLD;

    public Step(Waypoint w1, Waypoint w2) {
        start = w1;
        end = w2;
        distance = distanceFunction.getDistance(w1.getLatitude(), w1.getLongitude(),
                w2.getLatitude(), w2.getLongitude());
        if (null != w1.getTime()) {
            duration = Duration.between(w1.getTime(), w2.getTime());
            speed = distance / duration.getSeconds();
            if (speed > speedThreshold) {
                movingTime = Duration.between(w1.getTime(), w2.getTime());
            } else {
                movingTime = Duration.ofSeconds(0);
            }

        } else {
            speed = 0;
            duration = Duration.ofSeconds(0);
            movingTime = Duration.ofSeconds(0);
        }
        double ramp = w2.getElevation() - w1.getElevation();
        grade = ramp / distance * 100;
        if (ramp > 0) {
            ascent = ramp;
        } else {
            descend = -ramp;
        }
//        avgHeight = (w1.getElevation() + w2.getElevation()) / 2;
//        if (null != w1.getTrackPointExtension() && null != w2.getTrackPointExtension()) {
//            avgCadence = (w1.getTrackPointExtension().getCadence() + w2.getTrackPointExtension().getCadence()) / 2;
//            avgHeartRate = (w1.getTrackPointExtension().getHeartRate() + w2.getTrackPointExtension().getHeartRate()) / 2;
//        }
    }

    public Step(Waypoint waypoint) {
        distance = 0;
        duration = Duration.ofSeconds(0);
        speed = 0;
        ascent = 0;
        grade = 0;
        movingTime = Duration.ofSeconds(0);
        start = waypoint;
        end = waypoint;
    }

    @Override
    public double getDistance() {
        return distance;
    }

    @Override
    public double getSpeed() {
        return speed;
    }

    @Override
    public Duration getDuration() {
        return duration;
    }

    @Override
    public Duration getMovingTime() {
        return movingTime;
    }

    @Override
    public double getCadence() {
        return end.getCadence();
    }

    @Override
    public double getHeartRate() {
        return end.getHeartRate();
    }

    @Override
    public double getElevation() {
        return end.getElevation();
    }

    @Override
    public double getGrade() {
        return grade;
    }

    @Override
    public double getAscent() {
        return ascent;
    }

    @Override
    public double getDescend() {
        return descend;
    }

    @Override
    public Waypoint getStart() {
        return start;
    }

    @Override
    public Waypoint getEnd() {
        return end;
    }
}
