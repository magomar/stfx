package net.deludobellico.stfx.analysis;

import net.deludobellico.stfx.model.gpx.PointInfoType;
import net.deludobellico.stfx.model.gpx.Track;
import net.deludobellico.stfx.model.gpx.TrackSegment;
import net.deludobellico.stfx.model.gpx.Waypoint;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * A chunk contains all the derived information about two waypoints in a track: {@link #firstPoint} and
 * {@link #lastPoint}. It includes many attributes: distance, speed, time passed, altitud gained or lossed and grade,
 * average height, cadence and heart rate. A basic chunk will contain information between two consecutive waypoints in
 * track. However, a chunk can contain information between two arbitrary waypoints in a track. Actually, the information
 * about an entire track is summarized in a chunk between the initial and the final waypoints of the track.
 */
public class Chunk {
    private double distance; //meters
    private double speed; //meters / second
    private Duration duration;
    private Duration movingTime;
    private double grade; // percentual
    private double ascent; // absolute positive increment of height
    private double descend; // absolute negative increment of height
    private final Waypoint firstPoint;
    private final Waypoint lastPoint;
    private List<Chunk> chunks = new ArrayList<>();
    private double avgHeight;
    private double avgCadence;
    private double avgHeartRate;
    private GPSDistanceFunction distanceFunction = TrackAnalysisSettings.DEFAULT_DISTANCE_FUNCTION;
    private double speedThreshold = TrackAnalysisSettings.DEFAULT_SPEED_THRESHOLD;
    private double distanceThreshold = TrackAnalysisSettings.DEFAULT_DISTANCE_THRESHOLD;

    /**
     * Constructor taking two {@link Waypoint], a {@link DistanceFunction}, and a speed threshold. This chunk represents
     * the information between two waypoints within a {link TrackSegment}. In order to compute a valid duration, the second
     * waypoint must come after the first waypoint, that is, they must be temporally sorted.
     *
     * @param w1
     * @param w2
     * @param distanceFunction
     * @param speedThreshold
     */
    public Chunk(Waypoint w1, Waypoint w2, DistanceFunction distanceFunction, double speedThreshold) {
        firstPoint = w1;
        lastPoint = w2;
        distance = distanceFunction.getDistance(w1.getLatitude(), w1.getLongitude(),
                w2.getLatitude(), w2.getLongitude());
        if (null != w1.getTime()) {
            duration = Duration.between(w1.getTime(), w2.getTime());
            speed = distance / duration.getSeconds();
            if (speed > speedThreshold) {
                movingTime = Duration.between(w1.getTime(), w2.getTime());
            } else {
                movingTime = Duration.ofSeconds(0);
            }

        } else {
            speed = 0;
            duration = Duration.ofSeconds(0);
            movingTime = Duration.ofSeconds(0);
        }
        double ramp = w2.getElevation() - w1.getElevation();
        grade = ramp / distance * 100;
        if (ramp > 0) {
            ascent = ramp;
        } else {
            descend = -ramp;
        }
        avgHeight = (w1.getElevation() + w2.getElevation()) / 2;
        if (w1.hasInfoType(PointInfoType.CADENCE))
            avgCadence = (w1.getCadence() + w2.getCadence()) / 2;
        if (w1.hasInfoType(PointInfoType.HEART_RATE))
            avgHeartRate = (w1.getHeartRate() + w2.getHeartRate()) / 2;
    }

    /**
     * Constructor taking a single {@link Waypoint]. This chunk is used to represent the information of the first waypoint of a
     * track segment. This constructor is useful to represent exclusively the first point of track or track segment. The
     * result is a chunk with all attributes set to 0, where the first point is equal to the second  point.
     *
     * @param waypoint
     * @see Waypoint
     */
    public Chunk(Waypoint waypoint) {
        duration = Duration.ofSeconds(0);
        movingTime = Duration.ofSeconds(0);
        firstPoint = waypoint;
        lastPoint = waypoint;
    }

    /**
     * Constructor taking an entire {@link TrackSegment} as argument. This chunk represents the aggregated information of all the waypoints in a track
     *
     * @param trackSegment
     * @see TrackSegment
     */
    public Chunk(TrackSegment trackSegment) {
        distance = 0;
        duration = Duration.ofSeconds(0);
        movingTime = Duration.ofSeconds(0);
        ascent = 0;
        descend = 0;
        List<Waypoint> waypoints = new ArrayList<>(trackSegment.getWaypoints());
        firstPoint = waypoints.get(0);
        lastPoint = waypoints.get(waypoints.size());
        Waypoint previous = firstPoint;
        chunks.add(new Chunk(firstPoint));
    }

    /**
     * Constructor taking an entire {@link Track}. This chunk represents the aggregated information of all the waypoints in the track.
     *
     * @param track
     */
    public Chunk(Track track) {
        distance = 0;
        duration = Duration.ofSeconds(0);
        movingTime = Duration.ofSeconds(0);
        ascent = 0;
        descend = 0;
        firstPoint = track.getStart();
        lastPoint = track.getEnd();
        Waypoint previous = firstPoint;
        chunks.add(new Chunk(firstPoint));
        for (TrackSegment trackSegment : track.getTrackSegments())
            for (Waypoint waypoint : trackSegment.getWaypoints()) {
                if (!waypoint.equals(firstPoint)) {
                    Chunk chunk = new Chunk(previous, waypoint, distanceFunction, speedThreshold);
                    previous = waypoint;
                    chunks.add(chunk);
                    duration = duration.plus(chunk.duration);
                    movingTime = movingTime.plus(chunk.movingTime);
                    distance += chunk.distance;
                    ascent += chunk.ascent;
                    descend += chunk.descend;
                }
            }
        speed = distance / getDuration().getSeconds();
    }

    /**
     * Gets the distance between the two waypoints
     * @return
     */
    public double getDistance() {
        return distance;
    }

    /**
     * Gets the speed between the two waypoints
     * @return
     */
    public double getSpeed() {
        return speed;
    }

    /**
     * Gets the total amount of time passed between registering the two waypoints
     * @return
     */
    public Duration getDuration() {
        return duration;
    }
    /**
     * Gets the grade or ramp between the two waypoints.  The grade is the ratio between the change in elevation and
     * the distance between the two points, expressed in percentage.
     * @return
     */
    public double getGrade() {
        return grade;
    }

    /**
     * Gets the absolute increment of elevation between the two waypoints
     * @return the elevation of the last point minus the elevation of the first point if the last point is higher than
     * the first one, 0 otherwise.
     */
    public double getAscent() {
        return ascent;
    }

    /**
     * Gets the absolute decrement of elevation between the two waypoints
     * @return the elevation of the first point minus the elevation of the last point if the first point is higher than
     * the last one, 0 otherwise.
     */
    public double getDescend() {
        return descend;
    }

    /**
     * Gets the total amount of time passed between registering the two waypoints
     * @return
     */
    public Duration getMovingTime() {
        return movingTime;
    }

    /**
     * Gets the average cadence between the two points, that is the mean between the cadence
     * registered at the first point and the cadence registered at the second point.
     * @return
     */
    public double getAvgCadence() {
        return avgCadence;
    }

    /**
     * Gets the average heart rate between the two points (that is the mean between the heart rate
     * registered at the first point and the heart rate registered at the second point.
     * @return
     */
    public double getAvgHeartRate() {
        return avgHeartRate;
    }

    /**
     * Gets the average elevation between the two points, that is the mean between the elevation
     * at the first point and the elevation at the second point.
     * @return
     */
    public double getAvgHeight() {
        return avgHeight;
    }

    /**
     * Gets the first waypoint
     * @return
     */
    public Waypoint getFirstPoint() {
        return firstPoint;
    }

    /**
     * Gets the last waypoint
     * @return
     */
    public Waypoint getLastPoint() {
        return lastPoint;
    }


    public List<Chunk> getChunks() {
        return chunks;
    }
}
