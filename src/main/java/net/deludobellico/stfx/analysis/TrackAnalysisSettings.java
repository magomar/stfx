package net.deludobellico.stfx.analysis;

/**
 *
 * Default values for several parameters of the analysis algorithms.
 * @see TrackData
 * @see Chunk
 * @see DistanceFunction
 */
public interface TrackAnalysisSettings {
    GPSDistanceFunction DEFAULT_DISTANCE_FUNCTION = GPSDistanceFunction.HAVERSINE;
    double DEFAULT_SPEED_THRESHOLD = 0.1;
    double DEFAULT_DISTANCE_THRESHOLD = 100.0;
    int DEFAULT_HEART_RATE_THRESHOLD = 40;
    int DEFAULT_WAYPOINT_RESOLUTION = 1;
}
