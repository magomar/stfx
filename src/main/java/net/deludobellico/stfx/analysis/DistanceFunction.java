package net.deludobellico.stfx.analysis;

import java.math.BigDecimal;

/**
 *
 * Characterizes a function used to compute a distance between two geographical points
 */
public interface DistanceFunction {
    /**
     * Gets the distance between two geographical points, specified by latitud and longitud degrees
     *
     * @param latitude1
     * @param longitude1
     * @param latitude2
     * @param longitude2
     * @return
     */
    double getDistance(double latitude1, double longitude1, double latitude2, double longitude2);
}
